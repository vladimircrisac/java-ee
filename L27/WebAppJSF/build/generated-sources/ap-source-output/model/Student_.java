package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-04-23T21:04:40")
@StaticMetamodel(Student.class)
public class Student_ { 

    public static volatile SingularAttribute<Student, Long> studentId;
    public static volatile SingularAttribute<Student, String> addressZipcode;
    public static volatile SingularAttribute<Student, String> addressStreet;
    public static volatile SingularAttribute<Student, String> studentName;
    public static volatile SingularAttribute<Student, String> addressState;
    public static volatile SingularAttribute<Student, String> addressCity;

}