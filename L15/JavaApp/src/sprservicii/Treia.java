/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sprservicii;

/**
 *
 * @author iucosoft
 */
public class Treia {

    private Doua doua;

    public Doua getDoua() {
        return doua;
    }

    public void setDoua(Doua doua) {
        this.doua = doua;
    }

    @Override
    public String toString() {
        return "Treia{" + "doua=" + doua + '}';
    }

    public String getText() {
        return doua.getPrima().getText();
    }
}
