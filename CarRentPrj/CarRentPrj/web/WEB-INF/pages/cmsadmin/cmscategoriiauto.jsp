<%--
    Document   : categoriiauto
    Created on : Feb 21, 2019, 8:12:12 PM
    Author     : iucosoft
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entitati.Automobile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table, th, tr, td { border: 1px solid black; 
                                border-collapse: collapse;
                                padding: 5px}
        </style>
    </head>
    <body>
        <h1>Categoriile auto ADMIN!</h1>
        <p><a href="listaautomobileserv">Toate automobilele | </a>
            <a href="broneazaserv">Broneaza |</a>
            <a href="contacteserv">Contacte</a></p>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Marca</th>
                    <th>Model</th>
                    <th>Anul producerii</th>
                    <th>Tip caroserie</th>
                    <th>Pret chirie</th>
                </tr>
            </thead>
            <%
                List<Automobile> listByCategorie = (List<Automobile>) request.getAttribute("listByCategorie");
                int i = 0;
                for (Automobile auto : listByCategorie) {
            %>
            <tbody>
                <tr>
                    <td><%=i++%></td>
                    <td><%=auto.getIdAuto()%></td>
                    <td><%=auto.getMarca()%></td>
                    <td><%=auto.getModel()%></td>
                    <td><%=auto.getAnulProducerii()%></td>
                    <td><%=auto.getTipulCaroseriei()%></td>
                    <td><%=auto.getPretChirie()%></td>
                </tr>
            </tbody>
            <%
                }
            %>

        </table>
    </body>
</html>
