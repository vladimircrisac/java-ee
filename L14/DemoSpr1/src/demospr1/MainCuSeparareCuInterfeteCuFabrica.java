/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iucosoft
 */
public class MainCuSeparareCuInterfeteCuFabrica {

    public static void main(String[] args) {

        try {
            FabricaDeTexte fabrica = FabricaDeTexte.getInstance();

            TextProvider provider = fabrica.getProvider();

//        TextProvider provider = new TextProviderConsola();
//        TextRenderer renderer = new TextRendererConsola();
//        TextProvider provider = new TextProviderDialog();
//        TextRenderer renderer = new TextRendererDialog();
            TextRenderer renderer = fabrica.getRenderer();

            renderer.setProvider(provider);
            renderer.render();
        } catch (Exception ex) {
            Logger.getLogger(MainCuSeparareCuInterfeteCuFabrica.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
