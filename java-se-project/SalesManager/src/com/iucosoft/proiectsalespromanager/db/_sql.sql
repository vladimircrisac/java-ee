-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: salespromanager
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clienti`
--

DROP TABLE IF EXISTS `clienti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clienti` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NUME_CLIENT` varchar(45) NOT NULL,
  `ADRESA_FIZICA` varchar(45) DEFAULT NULL,
  `ADRESA_JURIDICA` varchar(45) NOT NULL,
  `DIRECTOR` varchar(45) DEFAULT NULL,
  `NR_TELEFON` int(11) DEFAULT NULL,
  `EMAIL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clienti`
--

LOCK TABLES `clienti` WRITE;
/*!40000 ALTER TABLE `clienti` DISABLE KEYS */;
INSERT INTO `clienti` VALUES (1,'client1','af1','aj1','dir1',0,'email1'),(2,'client2','af2','aj2','dir2',0,NULL),(3,'client4','af4','aj4','dir4',44,'email4'),(4,'client5','af5','aj5','dir5',55,'email5');
/*!40000 ALTER TABLE `clienti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comanda_noua`
--

DROP TABLE IF EXISTS `comanda_noua`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comanda_noua` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_CLIENT` int(11) DEFAULT NULL,
  `ID_PRODUS` int(11) DEFAULT NULL,
  `NUME_PRODUS` varchar(45) DEFAULT NULL,
  `CANTITATEA` int(11) DEFAULT NULL,
  `PRET_TOTAL` double DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `DATA_INTOCMIRE` datetime DEFAULT NULL,
  `DATA_LIVRARE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_comenzi_1_idx` (`ID`),
  KEY `fk_comenzi_1_idx1` (`ID_PRODUS`),
  KEY `fk_comenzi_2_idx` (`ID_CLIENT`),
  KEY `fk_comenzi_3_idx` (`ID_USER`),
  CONSTRAINT `fk_comenzi_1` FOREIGN KEY (`ID_PRODUS`) REFERENCES `produse` (`ID_PRODUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comenzi_2` FOREIGN KEY (`ID_CLIENT`) REFERENCES `clienti` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comenzi_3` FOREIGN KEY (`ID_USER`) REFERENCES `useri` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comanda_noua`
--

LOCK TABLES `comanda_noua` WRITE;
/*!40000 ALTER TABLE `comanda_noua` DISABLE KEYS */;
INSERT INTO `comanda_noua` VALUES (9,1,4,'produs4',1,22,1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `comanda_noua` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comanda_produse`
--

DROP TABLE IF EXISTS `comanda_produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comanda_produse` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_CLIENT` int(11) DEFAULT NULL,
  `ID_PRODUS` int(11) DEFAULT NULL,
  `CANTITATEA` int(11) DEFAULT NULL,
  `ID_USER` int(11) DEFAULT NULL,
  `DATA_INTOCMIRE` datetime DEFAULT NULL,
  `DATA_LIVRARE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_lista_comenzi_1_idx` (`ID_CLIENT`),
  KEY `fk_comanda_produse_1_idx` (`ID_PRODUS`),
  KEY `fk_comanda_produse_comanda_noua_idx` (`DATA_INTOCMIRE`,`DATA_LIVRARE`,`ID_USER`,`ID_CLIENT`,`ID_PRODUS`),
  CONSTRAINT `fk_comanda_produse_1` FOREIGN KEY (`ID_PRODUS`) REFERENCES `produse` (`ID_PRODUS`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comanda_produse_clienti` FOREIGN KEY (`ID_CLIENT`) REFERENCES `clienti` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comanda_produse`
--

LOCK TABLES `comanda_produse` WRITE;
/*!40000 ALTER TABLE `comanda_produse` DISABLE KEYS */;
INSERT INTO `comanda_produse` VALUES (1,1,4,40,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,1,5,50,1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `comanda_produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_comenzi`
--

DROP TABLE IF EXISTS `lista_comenzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_comenzi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NUME_CLIENT` varchar(45) DEFAULT NULL,
  `TOTAL_PRODUSE` int(11) DEFAULT NULL,
  `SUMA_TOTALA` double DEFAULT NULL,
  `DATA_INTOCMIRE` datetime DEFAULT NULL,
  `DATA_LIVRARE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_lista_comenzi_1_idx` (`NUME_CLIENT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_comenzi`
--

LOCK TABLES `lista_comenzi` WRITE;
/*!40000 ALTER TABLE `lista_comenzi` DISABLE KEYS */;
/*!40000 ALTER TABLE `lista_comenzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produse`
--

DROP TABLE IF EXISTS `produse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produse` (
  `ID_PRODUS` int(11) NOT NULL AUTO_INCREMENT,
  `NUME_PRODUS` varchar(45) DEFAULT NULL,
  `BARCODE` int(14) DEFAULT NULL,
  `PRET` double DEFAULT NULL,
  `STOCK` int(11) DEFAULT NULL,
  `PRODUCATOR` varchar(50) DEFAULT NULL,
  `COMENTARIU` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID_PRODUS`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produse`
--

LOCK TABLES `produse` WRITE;
/*!40000 ALTER TABLE `produse` DISABLE KEYS */;
INSERT INTO `produse` VALUES (4,'produs4',4444,22,NULL,'producator4','com4'),(5,'produs1',1111,11,NULL,'producator1','com1'),(6,'produs2',2222,22,NULL,'producator2','com2'),(8,'produs6',6666,66,26,'producator1','com1'),(9,'produs9',9999,99,40,'producator1','coment');
/*!40000 ALTER TABLE `produse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useri`
--

DROP TABLE IF EXISTS `useri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useri` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(45) DEFAULT NULL,
  `PASSWORD` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useri`
--

LOCK TABLES `useri` WRITE;
/*!40000 ALTER TABLE `useri` DISABLE KEYS */;
INSERT INTO `useri` VALUES (1,'user1','1234');
/*!40000 ALTER TABLE `useri` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zzzproduse_comanda`
--

DROP TABLE IF EXISTS `zzzproduse_comanda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zzzproduse_comanda` (
  `ID_COMANDA` int(11) NOT NULL,
  `ID_PRODUS` int(11) NOT NULL,
  `CANTITATEA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zzzproduse_comanda`
--

LOCK TABLES `zzzproduse_comanda` WRITE;
/*!40000 ALTER TABLE `zzzproduse_comanda` DISABLE KEYS */;
/*!40000 ALTER TABLE `zzzproduse_comanda` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-19 21:28:57
