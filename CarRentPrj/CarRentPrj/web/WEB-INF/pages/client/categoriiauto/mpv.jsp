<%-- 
    Document   : mpv
    Created on : Feb 24, 2019, 2:17:28 PM
    Author     : denis
--%>

<%@page import="entitati.Automobile"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
          <style>
            table, th, tr, td { border: 1px solid black; 
            border-collapse: collapse;
            padding: 5px}
        </style>
    </head>
    <body>
        <h1>MPV!</h1>
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Marca</th>
                    <th>Model</th>
                    <th>Anul producerii</th>
                    <th>Tip caroserie</th>
                    <th>Pret chirie</th>
                </tr>
            </thead>
            <%
                List<Automobile> listaMpv = (List<Automobile>) request.getAttribute("automobileMpv");
                int i = 0;
                for (Automobile auto : listaMpv) {
            %>
            <tbody>
                <tr>
                    <td><%=i++%></td>
                    <td><%=auto.getIdAuto()%></td>
                    <td><%=auto.getMarca()%></td>
                    <td><%=auto.getModel()%></td>
                    <td><%=auto.getAnulProducerii()%></td>
                    <td><%=auto.getTipulCaroseriei()%></td>
                    <td><%=auto.getPretChirie()%></td>
                </tr>
            </tbody>
            <%
                }
            %>

        </table>
    </body>
</html>
