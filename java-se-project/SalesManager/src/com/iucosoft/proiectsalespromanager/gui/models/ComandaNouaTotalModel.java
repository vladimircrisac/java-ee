/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class ComandaNouaTotalModel extends DefaultTableModel {

    String[] columns = {"Total produse", "Total spre achitare"};

    public ComandaNouaTotalModel(int numberOfRows, double sumaTotala) {

        Vector row = new Vector();
        row.addElement(numberOfRows);
        row.addElement(sumaTotala);
        super.dataVector.add(row);
    }
}
