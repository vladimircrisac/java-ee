package com.iucosoft.proiectsalespromanager.fileservicii.implementare.Clienti;

import com.iucosoft.proiectsalespromanager.entitati.Client;
import com.iucosoft.proiectsalespromanager.fileservicii.AbstractFileIOClient;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ClientFileIOXlsImp extends AbstractFileIOClient {

    
    @Override
    public void write(Collection<Client> coll, String fileDest) {
    
        try (
            FileOutputStream fos = new FileOutputStream(fileDest);
            Workbook wb = new XSSFWorkbook();)
        { 
         Sheet clientiSheet = wb.createSheet("Clienti");
         
            int rowIndex = 0;
            for (Client client : coll) {
                Row row = clientiSheet.createRow(rowIndex++);
                int cellIndex = 1;
                
                row.createCell(rowIndex).setCellValue(client.getId());
                row.createCell(rowIndex).setCellValue(client.getNumeClient());
                row.createCell(rowIndex).setCellValue(client.getAdresaFizica());
                row.createCell(rowIndex).setCellValue(client.getAdresaJuridica());
                row.createCell(rowIndex).setCellValue(client.getDirector());
                row.createCell(rowIndex).setCellValue(client.getTel());
                row.createCell(rowIndex).setCellValue(client.getEmail());
            }
            wb.write(fos);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
    
    
    @Override
    public Collection<Client> read(String fileSrc) {
        Collection<Client> collClient = new ArrayList<>();
        try (
            FileInputStream fis = new FileInputStream(fileSrc);
            Workbook wb = new XSSFWorkbook(fis);
                ){
            int numberOfSheets = wb.getNumberOfSheets();
            for (int i = 0; i < numberOfSheets; i++) {
                Sheet sheet = wb.getSheetAt(i);
                Iterator rowIterator = sheet.iterator();
                
                if (rowIterator.hasNext()) {
                    rowIterator.next();
                }
                
                while(rowIterator.hasNext()){
                    Client client = new Client();
                    Row row = (Row) rowIterator.next();
                    Iterator cellIterator = row.iterator();
                    while (cellIterator.hasNext()){
                       Cell cell = (Cell) cellIterator.next();
                        if (cell.getColumnIndex() == 0) {
                            client.setId((int) cell.getNumericCellValue());
                        } else if (cell.getColumnIndex() == 1){
                            client.setNumeClient(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == 2){
                            client.setAdresaFizica(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == 3){
                            client.setAdresaJuridica(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == 4) {
                            client.setDirector(cell.getStringCellValue());
                        } else if (cell.getColumnIndex() == 5) {
                            client.setTel((int) cell.getNumericCellValue());
                        } else if (cell.getColumnIndex() == 6) {
                            client.setEmail(cell.getStringCellValue());
                        }
                    }
                    collClient.add(client);
                }
            }
        
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return collClient;
    }
    
    public static void main(String[] args) {
        String fileName = "clienti.xls";
        
        ClientFileIOSerImp fileServiciu = new ClientFileIOSerImp();
        
        Collection<Client> coll = new ArrayList<>();
        coll.add(new Client(0, "numeClient1", "adresaFizica1", "adresaJuridica1", "director1", 1111, "email1"));
        coll.add(new Client(1, "numeClient2", "adresaFizica2", "adresaJuridica2", "director2", 2222, "email2"));
        
        System.out.println("scrie");
        fileServiciu.write(coll, fileName);
        System.out.println("scriere terminata in "+fileName+"\n");
        
        System.out.println("citire din fisier "+fileName);
        Collection<Client> collClientCitit = fileServiciu.read(fileName);
        for (Client client : collClientCitit) {
            System.out.println("idClient = "+client.getId()+
                               " numeClient = "+client.getNumeClient()+
                               " adresaFizica = "+client.getAdresaFizica()+
                               " adresaJuridica = "+client.getAdresaJuridica()+
                               " director = "+client.getDirector()+
                               " nrTel  = "+client.getTel()+
                               " email  = "+client.getEmail());
        }
        System.out.println("citire finisata.");
   }
}
