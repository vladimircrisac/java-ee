///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.javabycode.springmvc.dao;
//
//import com.javabycode.HibernateUtil;
//import com.javabycode.springmvc.model.University;
//import java.util.ArrayList;
//import java.util.List;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//import org.springframework.stereotype.Repository;
//
///**
// *
// * @author iucosoft
// */
//@Repository ("univerityDao")
//public class UniversityDaoImpl implements UniversityDao {
//
//    @Override
//    public University findById(int id) {
//
//        University univer = null;
//        Session sess = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = null;
//
//        try {
//            t = sess.beginTransaction();
//
//            univer = (University) sess.get(University.class, id);
//
//            t.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            t.rollback();
//        } finally {
//            sess.close();
//        }
//        return univer;
//    }
//
//    @Override
//    public List<University> findAll() {
//
//        List<University> listaUniver = new ArrayList<>();
//        Session sess = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = null;
//
//        try {
//            t = sess.beginTransaction();
//
//            listaUniver = sess.createQuery("from university").list();
//
//            t.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            t.rollback();
//        } finally {
//            sess.close();
//        }
//
//        return listaUniver;
//    }
//
//    @Override
//    public void delete(University univer) {
//        
//        Session sess = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = null;
//        
//        try {
//            t = sess.beginTransaction();
//            
//            sess.delete(univer);
//            
//            t.commit();
//            
//        } catch (Exception e) {
//            e.printStackTrace();
//            t.rollback();
//        } finally {
//            sess.close();
//        }
//        
//    }
//
//    @Override
//    public void update(University univer) {
//        
//        Session sess = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = null;
//        
//        try {
//            t = sess.beginTransaction();
//            
//            University univerDB = (University) sess.get(University.class, univer.getId());
//            univerDB.setAddress(univer.getAddress());
//            univerDB.setName(univer.getName());
//            
//            sess.update(univer);
//            
//            t.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            t.rollback();
//        } finally {
//            sess.close();
//        }
//    }
//
//    @Override
//    public void save(University univer) {
//        Integer id = null;
//        Session sess = HibernateUtil.getSessionFactory().openSession();
//        Transaction t = null;
//        
//        try {
//            t = sess.beginTransaction();
//            
//            id = (Integer) sess.save(univer);
//            
//            t.commit();
//        } catch (Exception e) {
//            e.printStackTrace();
//            t.rollback();
//        } finally {
//            sess.close();
//        }
//    }
//
//}
