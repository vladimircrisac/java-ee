/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

import java.util.Scanner;

/**
 *
 * @author iucosoft
 */
class TextProviderConsola implements TextProvider {

    private String text;

    private Scanner sc;

    public TextProviderConsola() {
        sc = new Scanner(System.in);
    }

    public String getText() {
        if (sc != null) {
            System.out.println("da un text");
            text = sc.nextLine();
        } else {
            System.out.println("sc este null, text ia valoarea implicita");
            text = "text de proba!";
        }
        return text;
    }

}
