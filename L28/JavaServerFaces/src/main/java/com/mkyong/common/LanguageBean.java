package com.mkyong.common;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

@ManagedBean
@SessionScoped
public class LanguageBean {
    
   private Map<String, Object> languageMap;
    
    @PostConstruct
    public void initLanguages(){
        languageMap = new HashMap<String, Object>();
        languageMap.put("English", Locale.ENGLISH);
        languageMap.put("Romana", new Locale("ro", "RO"));
    }
    
    public void languageChangedListener(ValueChangeEvent event){
        String selectedLanguage = event.getNewValue().toString();
        for(Entry<String, Object> entry: languageMap.entrySet()){
            if (entry.getValue().toString().equals(selectedLanguage)) {
                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) entry.getValue());
            }
            
        }
    }

    public Map<String, Object> getLanguageMap() {
        return languageMap;
    }

    public void setLanguageMap(Map<String, Object> languageMap) {
        this.languageMap = languageMap;
    }
    
    
}
