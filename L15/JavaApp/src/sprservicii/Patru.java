/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sprservicii;

/**
 *
 * @author iucosoft
 */
public class Patru {

    private Treia treia;

    public Treia getTreia() {
        return treia;
    }

    public void setTreia(Treia treia) {
        this.treia = treia;
    }

    @Override
    public String toString() {
        return "Patru{" + "treia=" + treia + '}';
    }

    public String showTextToUpperCase() {
        return treia.getText().toUpperCase();
    }

}
