
package com.vaannila.student;

/**
 *
 * @author iucosoftmain
 */

import apphiber1annotatii.util.HibernateUtil;
    import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;


public class TestManyToMany {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			
			Set<Course> courses = new HashSet<Course>();
			courses.add(new Course("house"));
			courses.add(new Course("mobile"));
			
			Student student = new Student ("Eswar", courses);
			session.save(student);
			
			transaction.commit();
                        
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
                        HibernateUtil.shutDown();
		}

	}


}
