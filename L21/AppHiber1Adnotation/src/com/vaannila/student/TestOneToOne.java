/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaannila.student;

import apphiber1adnotation.util.HibernateUtil;
import com.vaannila.address.Address;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoft
 */
public class TestOneToOne {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Address address1 = new Address("OMR Road", "Chennai", "TN", "600097");
            Address address2 = new Address("Ring Road", "Banglore", "Karnataka", "560000");
            Student student1 = new Student("Eswar", address1);
            Student student2 = new Student("Joe", address2);
            session.save(student1);
            session.save(student2);

            Student studentDB = (Student) session.get(Student.class, 1L);
            session.delete(studentDB);

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
            HibernateUtil.shutDown();
        }

    }
}
