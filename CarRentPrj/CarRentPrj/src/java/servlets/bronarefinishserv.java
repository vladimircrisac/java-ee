/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import dao.AutomobileDaoImpl;
import dao.AutomobileDaoIntf;
import dao.BronareDaoImpl;
import dao.BronareDaoIntf;
import entitati.Automobile;
import entitati.Bronare;
import entitati.Clienti;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author iucosoft
 */
public class bronarefinishserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        DataSource ds = (DataSource) request.getServletContext().getAttribute("datasouorce");
        BronareDaoIntf bronare = new BronareDaoImpl(ds);
        String marcaAuto = request.getParameter("marcaAuto");
        String modelAuto = request.getParameter("modelAuto");
        String startDate = request.getParameter("bronareStartDate");
        String endDate = request.getParameter("bronareEndDate");
        String numeClient = request.getParameter("bronareNumeClient");
        String prenumeClient = request.getParameter("bronarePrenumeClient");
        String experientaStr = request.getParameter("bronareExperientaClient");
        String comentariu = request.getParameter("bronareComentariuClient");
        int experienta = Integer.parseInt(experientaStr);
        Bronare bronareAuto = new Bronare(modelAuto, marcaAuto, LocalDate.MAX, LocalDate.MAX, numeClient, prenumeClient, experienta, comentariu);
        
        
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
