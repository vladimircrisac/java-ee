package com.iucosoft.proiectsalespromanager.fileservicii;

import java.util.Collection;

public interface MyWritable<T> {

    void write(Collection<T> coll, String fileDest);

}
