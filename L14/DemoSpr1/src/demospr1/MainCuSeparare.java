/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

/**
 *
 * @author iucosoft
 */
public class MainCuSeparare {

    public static void main(String[] args) {

//        TextProviderConsola provider = new TextProviderConsola();
//        TextRendererConsola renderer = new TextRendererConsola();
        TextProviderDialog provider = new TextProviderDialog();
        TextRendererDialog renderer = new TextRendererDialog();

        renderer.setProvider(provider);
        renderer.render();

    }

}
