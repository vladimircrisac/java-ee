/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

import javax.swing.JOptionPane;

/**
 *
 * @author iucosoft
 */
public class TextRendererDialog implements TextRenderer {

//    private TextProviderDialog provider;
    private TextProvider provider;

//    void setProvider(TextProviderDialog provider) {
//        this.provider = provider;
//    }
    public void setProvider(TextProvider provider) {
        this.provider = provider;
    }

    public void render() {
        if (provider == null) {
            JOptionPane.showConfirmDialog(null, "provider este null!");
            throw new RuntimeException("provider is null!");
        }
        JOptionPane.showMessageDialog(null, provider.getText());
    }

}
