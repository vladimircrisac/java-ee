/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javabycode.springmvc.service;

import com.javabycode.springmvc.model.University;
import java.util.List;

/**
 *
 * @author iucosoft
 */
public interface UniversityService {

    University findById(int id);

    List<University> findAll();

    void delete(University univer);

    void update(University univer);

    void save(University univer);

}
