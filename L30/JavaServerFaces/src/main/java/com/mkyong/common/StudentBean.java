package com.mkyong.common;

import com.mkyong.common.model.Student;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class StudentBean {

    private List<Student> studentList;
    private Student studentForm;

    public String goToStudentsPage() {
        // code
        studentList = initMockStudentList();

        studentForm = new Student();
        // la returnare scriem denumirea la pagina .xhtml
        return "students-page";
    }

    public void saveStudent() {

        if (studentForm.getId() == 0) {

            // metoda care face autoincrement la ID
            int maxId = 0;
            for (Student st : studentList) {
                if (st.getId() > maxId) {
                    maxId = st.getId();
                }
            }
            // se face intai ++ la ID, apoi se afiseaza ID - ++maxId
            studentForm.setId(++maxId);

            studentList.add(studentForm);
        } else {
            // nu scrim nimic. 
            // Se face ca si cum Update student.
        }

        studentForm = new Student();
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The entry has been saved!"));

    }

    public void deleteStudent(Student studentToDelete) {
        studentList.remove(studentToDelete);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("The entry has been deleted!"));
    }

    public void editStudent(Student studentToEdit) {
        this.studentForm = studentToEdit;
        
    }

    public List initMockStudentList() {
        List studentList = new ArrayList<Student>();
        Student student1 = new Student(1, "Ion", "MD", "ST-1-2018-F1");
        studentList.add(student1);

        Student student2 = new Student(2, "Diana", "RO", "ST-2-2018-F1");
        studentList.add(student2);

        Student student3 = new Student(3, "Sergiu", "MD", "ST-3-2018-F1");
        studentList.add(student3);

        Student student4 = new Student(4, "Valera", "RU", "ST-4-2018-F1");
        studentList.add(student4);

        return studentList;

    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public Student getStudentForm() {
        return studentForm;
    }

    public void setStudentForm(Student studentForm) {
        this.studentForm = studentForm;
    }
    
    

}
