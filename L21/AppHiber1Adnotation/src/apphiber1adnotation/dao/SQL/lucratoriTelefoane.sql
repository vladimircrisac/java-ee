-- MySQL dump 10.13  Distrib 5.7.25, for Linux (i686)
--
-- Host: localhost    Database: hiberdb
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ADDRESS`
--

DROP TABLE IF EXISTS `ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ADDRESS` (
  `ADDRESS_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_CITY` varchar(50) NOT NULL,
  `ADDRESS_STATE` varchar(50) NOT NULL,
  `ADDRESS_STREET` varchar(250) NOT NULL,
  `ADDRESS_ZIPCODE` varchar(10) NOT NULL,
  PRIMARY KEY (`ADDRESS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ADDRESS`
--

LOCK TABLES `ADDRESS` WRITE;
/*!40000 ALTER TABLE `ADDRESS` DISABLE KEYS */;
/*!40000 ALTER TABLE `ADDRESS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LUCRATOR`
--

DROP TABLE IF EXISTS `LUCRATOR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LUCRATOR` (
  `LUCRATOR_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LUCRATOR_NAME` varchar(100) NOT NULL,
  PRIMARY KEY (`LUCRATOR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LUCRATOR`
--

LOCK TABLES `LUCRATOR` WRITE;
/*!40000 ALTER TABLE `LUCRATOR` DISABLE KEYS */;
INSERT INTO `LUCRATOR` VALUES (1,'Ion');
/*!40000 ALTER TABLE `LUCRATOR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LUCRATOR_NAME`
--

DROP TABLE IF EXISTS `LUCRATOR_NAME`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LUCRATOR_NAME` (
  `LUCRATOR_ID` bigint(20) NOT NULL,
  `PHONE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`LUCRATOR_ID`,`PHONE_ID`),
  UNIQUE KEY `UK_o6ce5mgqx96nijgfxqcg6995n` (`PHONE_ID`),
  CONSTRAINT `FK_ias576mgubv3bav41j8nsm1eg` FOREIGN KEY (`LUCRATOR_ID`) REFERENCES `LUCRATOR` (`LUCRATOR_ID`),
  CONSTRAINT `FK_o6ce5mgqx96nijgfxqcg6995n` FOREIGN KEY (`PHONE_ID`) REFERENCES `PHONE` (`PHONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LUCRATOR_NAME`
--

LOCK TABLES `LUCRATOR_NAME` WRITE;
/*!40000 ALTER TABLE `LUCRATOR_NAME` DISABLE KEYS */;
INSERT INTO `LUCRATOR_NAME` VALUES (1,1),(1,2);
/*!40000 ALTER TABLE `LUCRATOR_NAME` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PHONE`
--

DROP TABLE IF EXISTS `PHONE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHONE` (
  `PHONE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PHONE_NUMBER` varchar(15) NOT NULL,
  `PHONE_TYPE` varchar(10) NOT NULL,
  PRIMARY KEY (`PHONE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PHONE`
--

LOCK TABLES `PHONE` WRITE;
/*!40000 ALTER TABLE `PHONE` DISABLE KEYS */;
INSERT INTO `PHONE` VALUES (1,'987654321','mobile'),(2,'123456789','house');
/*!40000 ALTER TABLE `PHONE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `STUDENT`
--

DROP TABLE IF EXISTS `STUDENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `STUDENT` (
  `STUDENT_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STUDENT_NAME` varchar(100) NOT NULL,
  `studentAddress_ADDRESS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`STUDENT_ID`),
  KEY `FK_jma18fn8d3vh8p06d6xh6et0n` (`studentAddress_ADDRESS_ID`),
  CONSTRAINT `FK_jma18fn8d3vh8p06d6xh6et0n` FOREIGN KEY (`studentAddress_ADDRESS_ID`) REFERENCES `ADDRESS` (`ADDRESS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `STUDENT`
--

LOCK TABLES `STUDENT` WRITE;
/*!40000 ALTER TABLE `STUDENT` DISABLE KEYS */;
/*!40000 ALTER TABLE `STUDENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `useri`
--

DROP TABLE IF EXISTS `useri`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `useri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nume` varchar(45) DEFAULT NULL,
  `prenume` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `useri`
--

LOCK TABLES `useri` WRITE;
/*!40000 ALTER TABLE `useri` DISABLE KEYS */;
/*!40000 ALTER TABLE `useri` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-28 21:04:18
