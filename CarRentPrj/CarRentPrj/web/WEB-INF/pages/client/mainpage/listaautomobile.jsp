<%--
    Document   : listaautomobile
    Created on : Feb 20, 2019, 10:32:59 PM
    Author     : denis
--%>

<%@page import="java.util.List"%>
<%@page import="entitati.Automobile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    
        <h1>Lista automobile CLIENT!</h1>
        <p><a href="categoriiautoserv">Categotii |</a>
            <a href="bronareserv">Broneaza |</a>
            <a href="contacteserv">Contacte</a></p>
        <table border="1" cellpadding="5" cellspacing="0">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Marca</th>
                    <th>Model</th>
                    <th>Anul producerii</th>
                    <th>Tipul caroseriei</th>
                    <th>Pic</th>
                </tr>
            </thead>

            <%
                List<Automobile> lista = (List<Automobile>) request.getAttribute("listaAuto");
                int i = 0;
                for (Automobile auto : lista) {
            %>
            <tbody>
                <tr>
                    <td><%=i++%></td>
                    <td><%=auto.getIdAuto()%></td>
                    <td><%=auto.getMarca()%></td>
                    <td><%=auto.getModel()%></td>
                    <td><%=auto.getAnulProducerii()%></td>
                    <td><%=auto.getTipulCaroseriei()%></td>
                    <td><a href="veziimagineautoserv?id=<%=auto.getImagineAuto()%>">pic</a></td>
                </tr>
            </tbody>
       
        <%
            }

        %>
         </table>
    </body>
</html>
