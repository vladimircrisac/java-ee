/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javabycode.springmvc.dao;

import com.javabycode.springmvc.model.Student;
import com.javabycode.springmvc.model.University;
import java.io.Serializable;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author iucosoft
 */
@Repository("universityDao")
public class UniversityDaoImplAbstract extends AbstractDao<Integer, University> implements UniversityDao {

    public University findById(int id) {
        return getByKey(id);
    }

    @SuppressWarnings("unchecked")
    public List<University> findAll() {
        Criteria criteria = createEntityCriteria();
        return (List<University>) criteria.list();

    }

    public void update(University univer) {
        super.saveOrUpdate(univer);

    }

    public void save(University univer) {
        persist(univer);
    }
    
    	public void deleteUniverById(int id) {
		Query query = getSession().createSQLQuery("delete from university where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}
    

}
