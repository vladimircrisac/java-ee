package apphiber1annotatii.dao.impl;

import apphiber1annotatii.dao.MyHiberUserDaoIntf;
import apphiber1annotatii.entities.MyHiberUser;
import apphiber1annotatii.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoftmain
 */
public class MyHiberUserDaoImpl implements MyHiberUserDaoIntf{
    
    
    @Override
    public Integer save(MyHiberUser user) {
        
        Integer id=null;
        Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
            id = (Integer) session.save(user);
            ///////////
            
            tr.commit();
        } catch (Exception e) {
            
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
            e.printStackTrace();
        }finally{
            session.close();
        }
        return id;
    }

    @Override
    public void update(MyHiberUser user) {
        
        Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
            MyHiberUser userBD= (MyHiberUser) session.get(MyHiberUser.class, user.getId());
            
            userBD.setUsername( user.getUsername());
            userBD.setPassword(user.getPassword());
            
            session.update(userBD);
            ///////////
            
            tr.commit();
        } catch (Exception e) {
            
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
            e.printStackTrace();
        }finally{
            session.close();
        }
        
    }

    @Override
    public void  delete(MyHiberUser user) {
       Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
            session.delete(user);
            ///////////
            
            tr.commit();
        } catch (Exception e) {
            
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
            e.printStackTrace();
        } finally{
            session.close();
        }
    }

    
    @Override
    public MyHiberUser findById(Integer userId) {
       
        MyHiberUser user=null;
        
        Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
             user = (MyHiberUser) session.get(MyHiberUser.class,userId);
            ///////////
            tr.commit();
        } catch (Exception e) {
            
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
        e.printStackTrace();
        } finally{
            session.close();
        }
        return user;
    }

    @Override
    public List<MyHiberUser> findAll() {
        List<MyHiberUser>  lista = new ArrayList<>();
        Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
             lista = session.createQuery("from MyHiberUser").list();
            ///////////
            tr.commit();
        } catch (Exception e) {
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
            e.printStackTrace();
        }finally{
            session.close();
        }           
        return lista;
    }

    @Override
    public void delete(Integer userId) {
         Session session  = HibernateUtil.getSessionFactory().openSession();
        Transaction tr=null;
        try {
            tr = session.beginTransaction();
            ////////////
            MyHiberUser user =(MyHiberUser) session.get(MyHiberUser.class, userId);
            session.delete(user);
            ///////////
            
            tr.commit();
        } catch (Exception e) {
            
            if(tr!=null){
               tr.rollback();
               tr=null;
            }
            e.printStackTrace();
        } finally{
            session.close();
        }
        
    }

    
}
