package com.iucosoft.proiectsalespromanager.dao.impl;

import com.iucosoft.proiectsalespromanager.dao.ClientDaoIntf;
import com.iucosoft.proiectsalespromanager.db.MyDataSource;
import com.iucosoft.proiectsalespromanager.entitati.Client;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ClientDaoImp implements ClientDaoIntf {

    private static final Logger LOG = Logger.getLogger(ClientDaoIntf.class.getName());

    private MyDataSource mds = MyDataSource.getInstance();
    private Connection connection;

    @Override
    public void save(Client client) throws SQLException {
// INSERT INTO `salespromanager`.`clienti` (`NUME_CLIENT`, `ADRESA_FIZICA`, `ADRESA_JURIDICA`, `DIRECTOR`, `NR_TELEFON`, `EMAIL`) VALUES ('client3', 'af3', 'aj3', 'dir3', '3', 'email3');

        String sqlSave = "INSERT INTO `salespromanager`.`clienti` "
                + "(NUME_CLIENT, ADRESA_FIZICA, ADRESA_JURIDICA, "
                + "DIRECTOR, NR_TELEFON, EMAIL) VALUES ("
                + "'" + client.getNumeClient() + "',"
                + "'" + client.getAdresaFizica() + "', "
                + "'" + client.getAdresaJuridica() + "', "
                + "'" + client.getDirector() + "', "
                + client.getTel() + ", "
                + "'" + client.getEmail() + "');";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sqlSave);
        LOG.info("saved!");
        connection.close();

    }

    @Override
    public void update(Client client) throws SQLException {
        String sqlUpdate = "UPDATE `salespromanager`.`clienti` "
                + "SET `nume_client` = '" + client.getNumeClient() + "', "
                + "`adresa_fizica` = '" + client.getAdresaFizica() + "', "
                + "`adresa_juridica` = '" + client.getAdresaJuridica() + "', "
                + "`director` = '" + client.getDirector() + "', "
                + "`tel` = '" + client.getTel() + "', "
                + "`email` = '" + client.getEmail() + "', "
                + "WHERE (`id` = '" + client.getId() + "');";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sqlUpdate);
        LOG.info("updated!");
        connection.close();
    }

    @Override
    public void delete(Client client) throws SQLException {

        String sqlDelete = "DELETE FROM `salespromanager`.`clienti` "
                + "WHERE (`ID` = '" + client.getId() + "');";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        stat.executeUpdate(sqlDelete);
        LOG.info("deleted!");
        connection.close();
    }

    @Override
    public Client findByID(int id) throws SQLException {
        Client client = null;
        String sqlFindByID = "SELECT * FROM `salespromanager`.`clienti` WHERE `id` = " + id;

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlFindByID);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public List<Client> findAll() throws SQLException {
        List<Client> listaClienti = new ArrayList<>();

        String sqlFindAll = "SELECT * FROM `salespromanager`.`clienti` ORDER BY nume_client ";
        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlFindAll);

        while (rs.next()) {
            Client client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));

            listaClienti.add(client);
        }
        connection.close();
        return listaClienti;
    }

    @Override
    public Client findByNumeClient(String numeClient) throws SQLException {

        Client client = null;
        String sqlfindByNumeClient = "SELECT * FROM `salespromanager`.`clienti` WHERE `NUME_CLIENT` = '" + numeClient + "'";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByNumeClient);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public Client findByAdresaFizica(String adresaFizica) throws SQLException {
        Client client = null;
        String sqlfindByAdresaFizica = "SELECT * FROM `salespromanager`.`clienti` WHERE `ADRESA_FIZICA` = '" + adresaFizica + "'";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByAdresaFizica);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public Client findByAdresaJuridica(String adresaJuridica) throws SQLException {
        Client client = null;
        String sqlfindByAdresaJuridica = "SELECT * FROM `salespromanager`.`clienti` WHERE `ADRESA_JURIDICA` = '" + adresaJuridica + "'";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByAdresaJuridica);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public Client findByDirector(String director) throws SQLException {
        Client client = null;
        String sqlfindByDirector = "SELECT * FROM `salespromanager`.`clienti` WHERE `DIRECTOR` = '" + director + "'";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByDirector);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public Client findByNrTel(int nrTel) throws SQLException {
        Client client = null;
        String sqlfindByNrTel = "SELECT * FROM `salespromanager`.`clienti` WHERE `NR_TELEFON` = " + nrTel;

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByNrTel);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

    @Override
    public Client findByEmail(String email) throws SQLException {
        Client client = null;
        String sqlfindByEmail = "SELECT * FROM `salespromanager`.`clienti` WHERE `EMAIL` = '" + email + "'";

        connection = mds.getConnection();
        Statement stat = connection.createStatement();
        ResultSet rs = stat.executeQuery(sqlfindByEmail);

        if (rs.next()) {
            client = new Client(rs.getInt(1), rs.getString(2), rs.getString(3),
                    rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7));
        }
        connection.close();
        return client;
    }

}
