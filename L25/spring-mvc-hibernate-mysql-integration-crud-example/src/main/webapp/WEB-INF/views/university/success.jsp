<%-- 
    Document   : success
    Created on : Apr 11, 2019, 8:26:24 PM
    Author     : iucosoft
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        ${success}
        <h1>Hello World!</h1>
        
        <a href="<c:url value='/university/list'/>">List of all universities</a>
    </body>
</html>
