<%-- 
    Document   : login
    Created on : Feb 24, 2019, 8:58:41 PM
    Author     : denis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>JSP Page</title>
    </head>
    <body>
        <p class="error">${error} </p>

        <form action="loginserv" method="POST">
            <div>
                <label for="username">Username</label>
                <input type="text" placeholder="Enter Username" name="username" required>

                <label for="password">Password</label>
                <input type="password" placeholder="Enter Password" name="password" required>

                <button type="submit">Login</button>
            </div>
        </form>
    </body>
</html>
