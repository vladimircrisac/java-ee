/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.dao;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Denis
 */
public interface ProdusDaoIntf {

    void save(Produs produs) throws SQLException; // done

    void update(Produs produs) throws SQLException; // done

    void delete(Produs produs) throws SQLException; // done

    Produs findByID(int id) throws SQLException; // done

    Produs findByNumeProdus(String numeProdus) throws SQLException;

    Produs findByBarcode(int barcode) throws SQLException;

    List<Produs> findByPret(double pret) throws SQLException;

    List<Produs> findByProducator(String producator) throws SQLException;

    List<Produs> findByComentariu(String comentariu) throws SQLException;

    List<Produs> findAll() throws SQLException; // show all - done

}
