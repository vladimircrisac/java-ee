/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javabycode.springmvc.service;

import com.javabycode.springmvc.dao.UniversityDao;
import com.javabycode.springmvc.model.University;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author iucosoft
 */
@Service("universityService")
@Transactional
public class UniversityServiceImpl implements UniversityService {

    @Autowired
    private UniversityDao univerityDao;

    @Override
    public University findById(int id) {
        return univerityDao.findById(id);
    }

    @Override
    public List<University> findAll() {
        return univerityDao.findAll();
    }

    @Override
    public void delete(University univer) {
        univerityDao.delete(univer);
    }

    @Override
    public void update(University univer) {
        univerityDao.update(univer);
    }

    @Override
    public void save(University univer) {
        univerityDao.save(univer);
    }

}
