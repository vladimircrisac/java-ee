/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

/**
 *
 * @author iucosoft
 */
public class MainCuSeparareCuInterfete {

    public static void main(String[] args) {

//        TextProvider provider = new TextProviderConsola();
        TextRenderer renderer = new TextRendererConsola();
        TextProvider provider = new TextProviderDialog();
//        TextRenderer renderer = new TextRendererDialog();

        renderer.setProvider(provider);
        renderer.render();

    }

}
