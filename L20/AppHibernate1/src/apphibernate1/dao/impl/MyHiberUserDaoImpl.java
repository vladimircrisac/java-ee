/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphibernate1.dao.impl;

import apphibernate1.dao.MyHiberUserDaoIntf;
import apphibernate1.mappings.MyHiberUser;
import apphibernate1.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoft
 */
public class MyHiberUserDaoImpl implements MyHiberUserDaoIntf {

    @Override
    public Integer save(MyHiberUser user) {

        Integer id = null;

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            id = (Integer) sess.save(user);
            ///////
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }

        return id;
    }

    @Override
    public void update(MyHiberUser user) {

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            MyHiberUser userDB = (MyHiberUser) sess.get(MyHiberUser.class, user.getId());
            userDB.setNume(user.getNume());
            userDB.setPrenume(user.getPrenume());

            sess.update(userDB);
            ///////
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    @Override
    public void delete(MyHiberUser user) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            sess.delete(user);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    @Override
    public MyHiberUser findById(Integer userId) {

        MyHiberUser hiberUser = null;

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            sess.update(hiberUser);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
        return hiberUser;
    }

    @Override
    public List<MyHiberUser> findAll() {

        List<MyHiberUser> hiberUserList = new ArrayList<>();

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            hiberUserList = sess.createQuery("from MyHiberUser").list();

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
        return hiberUserList;
    }

    @Override
    public void deleteUserId(Integer userId) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            MyHiberUser user = (MyHiberUser) sess.get(MyHiberUser.class, userId);
            sess.delete(userId);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

}
