/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entitati.Admin;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author denis
 */
public interface AdminDaoIntf {
    Admin getUserByUsernameAndPassword(String username, String password) throws IOException;
    
}
