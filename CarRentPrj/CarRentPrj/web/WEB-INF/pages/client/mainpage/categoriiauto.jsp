<%--
    Document   : categoriiauto
    Created on : Feb 21, 2019, 8:12:12 PM
    Author     : iucosoft
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="entitati.Automobile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--        <style>
                    table, th, tr, td { border: 1px solid black;
                                        border-collapse: collapse;
                                        padding: 5px}
                </style>-->
        <style>
            table {width: 75%; height: 50%}
        </style>
    </head>
    <body>
        <h1>Categoriile auto CLIENT!</h1>
        <p> <a href="listaautomobileserv">Toate automobilele | </a>
            <a href="bronareserv">Broneaza |</a>
            <a href="contacteserv">Contacte</a></p>

        <table>
            <tr>
                <td><a href="sedanserv">Sedan</a></td>
                <td><a href="hatchbackserv">Hatchback</a></td>
                <td><a href="mpvserv">MPV</a></td>
            </tr>

            <tr>
                <td><a href="suvserv">SUV</a></td>
                <td><a href="coupeserv">Coupe</a></td>
                <td><a href="cabrioletserv">Cabriolet</a></td>
            </tr>
        </table>

    </body>
</html>

