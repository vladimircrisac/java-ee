/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sprservicii;

/**
 *
 * @author iucosoft
 */
public class TestManual {

    public static void main(String[] args) {
        Prima p = new Prima();
        p.setText("text din prima");

        Doua d = new Doua();
        d.setPrima(p);

        Treia t = new Treia();
        t.setDoua(d);

//        System.out.println("text din prima prin trei este " + t.getDoua().getPrima().getText());
        System.out.println("text din prima prin trei este = " + t.getText());
    }

}
