package demospr1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class FabricaDeTexte {

    private TextProvider provider;
    private TextRenderer renderer;

    private String providerClassName;
    private String rendererClassName;

    private static FabricaDeTexte instance;

    static FabricaDeTexte getInstance() throws Exception {
        if (instance == null) {
            instance = new FabricaDeTexte();

        }
        return instance;
    }

    private FabricaDeTexte() throws Exception {
        loadProperties();
        init();
    }

    private void loadProperties() throws Exception {
        Properties props = new Properties();
        props.load(new FileReader("config.properties"));

        providerClassName = props.getProperty("provider");
        rendererClassName = props.getProperty("renderer");
    }

    private void init() throws Exception {
        provider = (TextProvider) Class.forName(providerClassName).newInstance();
        renderer = (TextRenderer) Class.forName(rendererClassName).newInstance();
    }

    TextProvider getProvider() {
        return provider;

    }

    TextRenderer getRenderer() {
        return renderer;
    }
}
