/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphiber1.dao.impl;
 
import apphiber1.mappings.MyHiberUser;
import apphiber1.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
 
/**
 *
 * @author iucosoftmain
 */
public class MyHiberUserDaoImplTestProf {
   
    private MyHiberUserDaoImpl instance = new MyHiberUserDaoImpl();
   
    @Before
    public void setUp() {
        //create table + add 1 row
        String sql = "CREATE TABLE `hiberbd`.`useri` (\n"
                + "  `id` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `username` VARCHAR(45) NULL,\n"
                + "  `password` VARCHAR(45) NULL,\n"
                + "  PRIMARY KEY (`id`));";
 
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = session.beginTransaction();
            ////////////
 
            session.createSQLQuery(sql).executeUpdate();
 
            session.save(new MyHiberUser("userTest1", "pass1"));
 
            ///////////
            tr.commit();
        } catch (Exception e) {
 
            if (tr != null) {
                tr.rollback();
                tr = null;
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
 
    }
 
   @After
    public void tearDown() {
        //drop table
 
        String sql = "DROP TABLE `hiberbd`.`useri`;";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = session.beginTransaction();
            ////////////
 
            session.createSQLQuery(sql).executeUpdate();
 
            ///////////
            tr.commit();
        } catch (Exception e) {
 
            if (tr != null) {
                tr.rollback();
                tr = null;
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
 
    /**
     * Test of save method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        MyHiberUser user = new MyHiberUser("user2", "password2");
 
        ////
        Integer expResult = 2;
        Integer result = instance.save(user);
        assertEquals(expResult, result);
       
        ///
        String expResultStr = "user2";
        String resultStr = instance.findById(2).getUsername();
        assertEquals(expResultStr, resultStr);
    }
 
    /**
     * Test of update method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        MyHiberUser user = instance.findById(1);
        user.setUsername("userTest2");
       
        instance.update(user);
       
        MyHiberUser updatedUser = instance.findById(1);
        String expString = "userTest2";
        String resultString = updatedUser.getUsername();
       
        System.out.println("rS = "+resultString);
       
        assertNotNull(resultString);
        assertEquals(expString, resultString);
    }
 
    /**
     * Test of delete method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");
        MyHiberUser user = instance.findById(1);
        instance.delete(user);
        Integer result = instance.findAll().size();
        Integer expResult = 0;
        assertEquals(expResult, result);
    }
 
    /**
     * Test of findById method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testFindById() {
        System.out.println("findById");
        Integer userId = 1;
        MyHiberUser expResult = new MyHiberUser("userTest1", "pass1");
        MyHiberUser result = instance.findById(userId);
        assertEquals(expResult, result);
     
    }
 
    /**
     * Test of findAll method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        Integer expResult = 1;
        Integer result = instance.findAll().size();
        assertEquals(expResult, result);
       
    }
 
}