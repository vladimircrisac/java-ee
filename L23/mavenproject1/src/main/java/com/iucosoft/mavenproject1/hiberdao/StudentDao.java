/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.mavenproject1.hiberdao;

import com.iucosoft.mavenproject1.entities.Student;
import com.iucosoft.mavenproject1.util.HibernateUtil;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoft
 */
public class StudentDao {
public List<Student> getAll() {
    Session sess = HibernateUtil.getSessionFactory().openSession();

    
        Transaction tr = null;

        tr = sess.beginTransaction();

        List<Student> listaStudenti = sess.createQuery("from Student").list();
        try {
            tr.commit();
        } catch (HibernateException he) {
            he.printStackTrace();
            tr.rollback();
        } finally {
            sess.close();
        }
        return listaStudenti;

    }

}
