/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.ComandaNoua;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class TableListaComenziModel extends DefaultTableModel {

    String[] columns = {"ID", "Nume client", "Adresa fizica", "Pret total",
        "ID User", "Data intocmire", "Data livrare"};

    public TableListaComenziModel(List<ComandaNoua> listaComenzi) {

        for (String col : columns) {
            super.addColumn(col);
        }

        for (ComandaNoua listCom : listaComenzi) {
            Vector row = new Vector();

            row.addElement(listCom.getId());
            row.addElement(listCom.getClient());
            row.addElement("ardesa");
            row.addElement("pret total");
            row.addElement("ID user");
            row.addElement("data intocmire");
            row.addElement("data livrare");

            super.dataVector.add(row);
        }

    }

}
