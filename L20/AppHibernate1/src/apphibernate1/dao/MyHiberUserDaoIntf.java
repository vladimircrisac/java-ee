/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphibernate1.dao;

import apphibernate1.mappings.MyHiberUser;
import java.util.List;

/**
 *
 * @author iucosoft
 */
public interface MyHiberUserDaoIntf {
    
    Integer save(MyHiberUser user);
    void update(MyHiberUser user);
    void delete (MyHiberUser user);
    void deleteUserId (Integer userId);
    
    
    MyHiberUser findById (Integer userId);
    List<MyHiberUser> findAll();
}
