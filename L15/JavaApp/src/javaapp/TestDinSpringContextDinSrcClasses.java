package javaapp;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sprservicii.Patru;

public class TestDinSpringContextDinSrcClasses {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

//        Treia t = (Treia) context.getBean("treia");
//        System.out.println(t.getText());
        Patru patra = (Patru) context.getBean("patru");
        System.out.println(patra.showTextToUpperCase() + " +++++ text cu majuscule !!");

    }

}
