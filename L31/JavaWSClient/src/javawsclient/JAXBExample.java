/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javawsclient;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JAXBExample {

    public static void main(String[] args) {

//        javaToXml();
//        System.out.println("acum invers - XML to Java");
//        javaListToXml();
        xmlTojavaList();

//        xmlToJava();

    }

    private static void javaListToXml() {
        Customer customer1 = new Customer();
        customer1.setId(1);
        customer1.setName("Cust111");
        customer1.setAge(111);

        Customer customer2 = new Customer();
        customer2.setId(2);
        customer2.setName("Cust2");
        customer2.setAge(222);

        Customer customer3 = new Customer();
        customer3.setId(3);
        customer3.setName("Cust333");
        customer3.setAge(333);

        Customer customer4 = new Customer();
        customer4.setId(4);
        customer4.setName("Cust444");
        customer4.setAge(444);

        List<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        customerList.add(customer4);

        CustomersList customersListXml = new CustomersList();
        customersListXml.setCustomerList(customerList);

        try {

            File file = new File("allcustomers.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(CustomersList.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(customersListXml, file);
            jaxbMarshaller.marshal(customersListXml, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    private static void javaToXml() {
        Customer customer = new Customer();
        customer.setId(100);
        customer.setName("mkyong");
        customer.setAge(29);

        try {

            File file = new File("javaToXml.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(customer, file);
            jaxbMarshaller.marshal(customer, System.out);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    private static void xmlToJava() {
        try {

            File file = new File("javaToXml.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(Customer.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Customer customer = (Customer) jaxbUnmarshaller.unmarshal(file);
            System.out.println(customer);

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

    private static void xmlTojavaList() {
        try {

            File file = new File("allcustomers.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(CustomersList.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            CustomersList customersList = (CustomersList) jaxbUnmarshaller.unmarshal(file);
            
            List<Customer> lista = customersList.getCustomerList();
            
            for (Customer c : lista) {
                System.out.println(c);
            }
            

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
