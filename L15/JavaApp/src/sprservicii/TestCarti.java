package sprservicii;

import dao.CarteDaoIntf;
import entitati.Carte;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestCarti {

    public static void main(String[] args) {
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

            CarteDaoIntf carteDao = (CarteDaoIntf) context.getBean("carteDao");

            // use
            Carte carteBD = carteDao.getById(1);
            System.out.println("carte cu id 1 " + carteBD);

            List<Carte> lista = carteDao.getAll();
            System.out.println("lista de carti!!!!!!!!!!!1");
            for (Carte c : lista) {
                System.out.println(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestCarti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
