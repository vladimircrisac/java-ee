/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphiber1adnotation.dao.impl;

import apphiber1adnotation.Entities.User;
import apphiber1adnotation.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author iucosoft
 */
public class UserDaoImplTest {

    private UserDaoImpl instance = new UserDaoImpl();

    @Before
    public void setUp() {
        //create table + add 1 row
        String sql = "CREATE TABLE useri (\n"
                + "  `id` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `nume` VARCHAR(45) NULL,\n"
                + "  `prenume` VARCHAR(45) NULL,\n"
                + "  PRIMARY KEY (`id`));";

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = session.beginTransaction();
            ////////////

            session.createSQLQuery(sql).executeUpdate();

            session.save(new User("userTest1", "pass1"));

            ///////////
            tr.commit();
        } catch (Exception e) {

            if (tr != null) {
                tr.rollback();
                tr = null;
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @After
    public void tearDown() {
        //drop table

        String sql = "DROP TABLE useri;";
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tr = null;
        try {
            tr = session.beginTransaction();
            ////////////

            session.createSQLQuery(sql).executeUpdate();

            ///////////
            tr.commit();
        } catch (Exception e) {

            if (tr != null) {
                tr.rollback();
                tr = null;
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    /**
     * Test of save method, of class UserDaoImpl.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        User user = new User("userTest2", "passwordTest2");
        Integer expResult = 2;
        Integer result = instance.save(user);
        assertEquals(expResult, result);

        String expResultStr = "userTest2";
        String resultStr = instance.findById(2).getNume();
        assertEquals(expResult, result);
    }

    /**
     * Test of update method, of class UserDaoImpl.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        User user = instance.findById(1);
        user.setNume("userTestUpdate2");

        instance.update(user);

        User updatedUser = instance.findById(1);
        String expString = "userTestUpdate2";
        String resultString = updatedUser.getNume();

        assertNotNull(resultString);
        assertEquals(expString, resultString);
    }

    /**
     * Test of delete method, of class UserDaoImpl.
     */
//    @Test
    public void testDelete() {
        System.out.println("delete");
        User user = instance.findById(1);
        instance.delete(user);

        Integer result = instance.findAll().size();
        Integer expResult = 0;
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteUserId method, of class UserDaoImpl.
     */
    @Test
    public void testDeleteUserId() {
        System.out.println("deleteUserId");
        User user = instance.findById(1);
        instance.delete(user);
        Integer result = instance.findAll().size();
        Integer expResult = 0;
        assertEquals(expResult, result);
    }

    /**
     * Test of findById method, of class UserDaoImpl.
     */
    @Test
    public void testFindById() {
        System.out.println("findById");
        Integer userId = 1;
        User expResult = new User("userTest1", "passTest1");
        User result = instance.findById(userId);
//        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class UserDaoImpl.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");
        Integer expResult = 1;
        Integer result = instance.findAll().size();
        assertEquals(expResult, result);
    }

}
