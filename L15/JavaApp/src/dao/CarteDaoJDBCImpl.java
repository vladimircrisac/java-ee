package dao;

import entitati.Carte;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author iucosoftmain
 */
public class CarteDaoJDBCImpl implements CarteDaoIntf {

    private DataSource dataSource;

//    public CarteDaoJDBCImpl(DataSource dataSource) {
//        this.dataSource = dataSource;
//    }
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Carte getById(int id) throws SQLException {

        Carte carte = null;

        try (
                Connection conn = dataSource.getConnection();
                Statement st = conn.createStatement();) {

            String sql = "SELECT * FROM CARTI WHERE id=" + id;

            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {

                String titlul = rs.getString(2);
                String autor = rs.getString(3);
                String isbn = rs.getString(4);
                int nrPagini = rs.getInt(5);
                double pret = rs.getDouble(6);

                byte[] imagine = null;

                String imagineNume = rs.getString(8);

                carte = new Carte(id, titlul, autor, isbn, nrPagini, pret, imagine, imagineNume);

            }

        } catch (Exception e) {

            e.printStackTrace();

            throw new SQLException(e.getMessage());
        }

        //ds.getConnection().close();
        return carte;
    }

    @Override
    public Carte getByIsbn(String isbn) throws SQLException {

        return null;
    }

    @Override
    public List<Carte> getAll() throws SQLException {
        List<Carte> lista = new ArrayList<>();

        Connection conn = dataSource.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM CARTI";

        ResultSet rs = st.executeQuery(sql);

        while (rs.next()) {

            int id = rs.getInt(1);
            String titlul = rs.getString(2);
            String autor = rs.getString(3);
            String isbn = rs.getString(4);
            int nrPagini = rs.getInt(5);
            double pret = rs.getDouble(6);
            byte[] imagine = null;

            Carte carte = new Carte(id, titlul, autor, isbn, nrPagini, pret, imagine);
            lista.add(carte);
        }

        dataSource.getConnection().close();
        return lista;
    }

    @Override
    public List<Carte> getAllByAutor(String autor) throws SQLException {
        return null;
    }

    @Override
    public List<Carte> getAllByContainIntitle(String titleCont) throws SQLException {

        return null;
    }

    @Override
    public void save(Carte carte) throws SQLException {
        Connection conn = dataSource.getConnection();

        String sql = "INSERT INTO CARTI (titlul, autor, isbn, nrPagini, pret) VALUES ( ?, ?, ?, ?,?);";

        PreparedStatement psmt = conn.prepareStatement(sql);

        psmt.setString(1, carte.getTitlul());
        psmt.setString(2, carte.getAutor());

        psmt.setString(3, carte.getIsbn());

        psmt.setInt(4, carte.getNrPagini());
        psmt.setDouble(5, carte.getPret());

// cu imaginea... +++
        psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
        dataSource.getConnection().close();

    }

    @Override
    public void update(Carte carte) throws SQLException {

        Connection conn = dataSource.getConnection();
        //UPDATE `ANGAJATIDB`.`CARTI` SET `titlul`='www', `autor`='fff', `isbn`='222', `nrPagini`='1', `pret`='11.11122' WHERE `id`='7';
        String sql = "UPDATE CARTI SET titlul=?, autor=?, isbn=?, nrPagini=?, pret=? WHERE id=?;";

        PreparedStatement psmt = conn.prepareStatement(sql);

        psmt.setString(1, carte.getTitlul());
        psmt.setString(2, carte.getAutor());

        psmt.setString(3, carte.getIsbn());

        psmt.setInt(4, carte.getNrPagini());
        psmt.setDouble(5, carte.getPret());
        psmt.setInt(6, carte.getId());

// cu imaginea... +++
        psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
        dataSource.getConnection().close();

    }

    @Override
    public void delete(Carte carte) throws SQLException {

        Connection conn = dataSource.getConnection();
        Statement st = conn.createStatement();

        String sql = "DELETE FROM CARTI WHERE id=" + carte.getId();

        st.executeUpdate(sql);
        dataSource.getConnection().close();
    }

    @Override
    public List<Carte> getFullAll() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCarteImg(int id, String imgName, byte[] imgBytes) throws SQLException {

        String sql = "UPDATE CARTI SET  imagine=?, imgName=?  WHERE id=?;";

        try (
                Connection conn = dataSource.getConnection();
                PreparedStatement psmt = conn.prepareStatement(sql);) {

            psmt.setBytes(1, imgBytes);
            psmt.setString(2, imgName);
            psmt.setInt(3, id);

// cu imaginea... +++
            psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
            //   dataSource.getConnection().close();

        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }

    }

}
