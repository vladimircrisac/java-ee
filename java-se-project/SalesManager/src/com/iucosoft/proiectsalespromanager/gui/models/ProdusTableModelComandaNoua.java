/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class ProdusTableModelComandaNoua extends DefaultTableModel {

    String[] columns = {"Nume produs", "Barcode", "Stock", "Pret"};

    public ProdusTableModelComandaNoua(List<Produs> listaProduse) {
        for (String col : columns) {
            super.addColumn(col);
        }

        for (Produs produs : listaProduse) {
            Vector row = new Vector();

            row.addElement(produs.getNumeProdus());
            row.addElement(produs.getBarCode());
            row.addElement(produs.getStock());
            row.addElement(produs.getPret());

            super.dataVector.add(row);
        }

    }
}
