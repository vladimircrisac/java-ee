/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sprservicii;

/**
 *
 * @author iucosoft
 */
public class Doua {

    private Prima prima;

    public Prima getPrima() {
        return prima;
    }

    public void setPrima(Prima prima) {
        this.prima = prima;
    }

    @Override
    public String toString() {
        return "Doua{" + "prima=" + prima + '}';
    }

}
