/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphiber1adnotation.dao.impl;

import apphiber1adnotation.Entities.User;
import apphiber1adnotation.util.HibernateUtil;
import apphiber1adnotation.dao.UserDaoIntf;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoft
 */
public class UserDaoImpl implements UserDaoIntf {

    @Override
    public Integer save(User user) {
        Integer id = null;

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            id = (Integer) sess.save(user);
            ///////
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }

        return id;
    }

    @Override
    public void update(User user) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            User userDB = (User) sess.get(User.class, user.getId());
            userDB.setNume(user.getNume());
            userDB.setPrenume(user.getPrenume());

            sess.update(userDB);
            ///////
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    @Override
    public void delete(User user) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            sess.delete(user);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    @Override
    public void deleteUserId(Integer userId) {
        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            User user = (User) sess.get(User.class, userId);
            sess.delete(userId);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    @Override
    public User findById(Integer userId) {
        User user = null;

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
           user =  (User) sess.get(User.class, userId);

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
        return user;
    }

    @Override
    public List<User> findAll() {
        List<User> hiberUserList = new ArrayList<>();

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            // pana aici totul e standart

            // doar aceasta metoda se schimba
            hiberUserList = sess.createQuery("from User").list();

            // de aici totul e standart
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }
        return hiberUserList;
    }
}
