/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class TableCosModel extends DefaultTableModel {

    String[] columns = {"Denumire produs", "Barcode", "Cantitate", "Pret"};

    public TableCosModel(List<Produs> adaugaProdus) {
        for (String col : columns) {
            super.addColumn(col);
        }

        for (Produs produs : adaugaProdus) {
            Vector row = new Vector();

            row.addElement(produs.getNumeProdus());
            row.addElement(produs.getBarCode());
            row.addElement(produs.getStock());
            row.addElement(produs.getPret()); // suma

            super.dataVector.add(row);
        }
    }

    @Override
    public Object getValueAt(int row, int column) {
        return super.getValueAt(row, column);
    }
}
