/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.common.model;

/**
 *
 * @author iucosoft
 */
public class Specialty {
    
    private int id;
    private String name;
    private int nrOfStudents;
    private String nameProfessor;

    public Specialty() {
    }

    public Specialty(int id, String name, int nrOfStudents, String nameProfessor) {
        this.id = id;
        this.name = name;
        this.nrOfStudents = nrOfStudents;
        this.nameProfessor = nameProfessor;
    }

    public String getNameProfessor() {
        return nameProfessor;
    }

    public void setNameProfessor(String nameProfessor) {
        this.nameProfessor = nameProfessor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNrOfStudents() {
        return nrOfStudents;
    }

    public void setNrOfStudents(int nrOfStudents) {
        this.nrOfStudents = nrOfStudents;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Specialty other = (Specialty) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Specialitate{" + "id=" + id + ", name=" + name + ", nrOfStudents=" + nrOfStudents + ", nameProfessor=" + nameProfessor + '}';
    }
    
    
    
}
