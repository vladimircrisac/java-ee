<%--
    Document   : listaautomobile
    Created on : Feb 20, 2019, 10:32:59 PM
    Author     : denis
--%>

<%@page import="java.util.List"%>
<%@page import="entitati.Automobile"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table, th, tr, td { border: 1px solid black;
                                border-collapse: collapse;
                                padding: 5px}
        </style>
    </head>
    <h1>Lista automobile ADMIN!</h1>
    <p><a href="categoriiautoserv">Categotii |</a>
        <a href="broneazaserv">Broneaza |</a>
        <a href="contacteserv">Contacte</a></p>

    <a href="cmsgotoeditautomobilserv?id=0">adauga automobil nou</a>

    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>ID</th>
                <th>Marca</th>
                <th>Model</th>
                <th>Anul producerii</th>
                <th>Tipul caroseriei</th>
                <th>Pret chirie</th>
                <th>EDIT</th>
                <th>DELETE</th>

            </tr>
        </thead>

        <%
            List<Automobile> listaAuto = (List<Automobile>) request.getAttribute("listaAutomobile");
            int i = 0;
            for (Automobile auto : listaAuto) {
        %>
        <tbody>
            <tr>
                <td><%=i++%></td>
                <td><%=auto.getIdAuto()%></td>
                <td><%=auto.getMarca()%></td>
                <td><%=auto.getModel()%></td>
                <td><%=auto.getAnulProducerii()%></td>
                <td><%=auto.getTipulCaroseriei()%></td>
                <td><%=auto.getPretChirie()%></td>
                <td><a href="cmsgotoeditautomobilserv?id=<%=auto.getIdAuto()%>">edit</a></td>
                <td><a href="cmsdeleteautomobilserv?id=<%=auto.getIdAuto()%>">DELETE</a></td>



            </tr>
        </tbody>

        <%
            }

        %>
    </table>
</body>
</html>
