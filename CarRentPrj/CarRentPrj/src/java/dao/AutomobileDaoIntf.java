/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entitati.Automobile;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author iucosoft
 */
public interface AutomobileDaoIntf {

    List<Automobile> getAll() throws SQLException;

    Automobile getById(int idAuto) throws SQLException;

    List<Automobile> getByCategorieSedan() throws SQLException;

    List<Automobile> getByCategorieHatchback() throws SQLException;

    List<Automobile> getByCategorieMPV() throws SQLException;

    List<Automobile> getByCategorieSUV() throws SQLException;

    List<Automobile> getByCategorieCoupe() throws SQLException;

    List<Automobile> getByCategorieCabriolet() throws SQLException;

    void save(Automobile automobilSave) throws SQLException;
    
    void delete(Automobile automobil) throws SQLException;

}
