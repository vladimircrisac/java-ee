Car Rent.

Este un web site pentru inchirierea automobilelor. Are 4 pagini principale:

1.Toate automobilele - pagina pe care se vor afisa toate automobilele pe care le are compania-prestatorul de servicii.
Se afiseaza panacand in forma de tabel (pana nu implementez un template gata facut). Pic - arata imaginea auto. Inca nu e facuta. Trebuie sa ma joc putin cu convertirea imaginii in String si salvarea ei in SQL.
a.se vor afisa in format preview: poza de format mic cu marca, modelul si pretul chiriei.
b. la click pe poza ea se va mari.

2.Categorii - pagina lista de categorii de automobile (tip caroserie).
a.la click pe categorie se va afisa lista cu toate automobilele din aceasta categorie in format din punctul 1.a.

3.Broneaza - aici clientul va alege marca si modelul auto din droplist, si va completa formularul pentru inchiriere.
nota* aici vreau sa fac modificari: 
a: pagina bronare se va sterge din menu principal. Va fi accesibila prin redirect de la lista cu automobile.
b: la fiecare automobil, pe langa poza, marca si model, vreau sa adaug butonul "broneaza", care va redirectiona clientul spre /bronareserv, unde marca si modelul auto deja va fi selectat.

4. Contacte - date despre companie, nr de telefoane, email.

-----------------

Frontend.

Vreau sa implementez un simplu template bootstrap, ca sa arate totul putin mai frumos si aranjat.

-----------------

Partea de administrare - CMS.

login: admin
pass: admin

pagina CMS Lista automobile ne duce la cmslistaautomobileserv si ne returneaza lista de automobile (ca si in partea Client) + 2 functii: Edit si Delete.
a: Edit -> gotoeditautomobilserv -> avem o forma de editare a informatiei despre automobil.
**functia Edit Automobil(save or update) nu functioneaza din cauza ca nu se face parsarea la Data (anul producerii) din Java in SQL si invers (parca am scris normal, dar tot nu lucreaza, hz..poate am scapat ceva..)
b: Delete - sterge automobilul din baza de date. (nu lucreaza din aceeasi cauza ca si Editul).

