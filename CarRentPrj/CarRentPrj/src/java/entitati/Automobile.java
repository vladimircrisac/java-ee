/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitati;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author denis
 */
public class Automobile {

    private int idAuto;
    private String marca;
    private String model;
    private LocalDate anulProducerii;
    private String tipulCaroseriei;
    private int pretChirie;
    private byte[] imagineAuto;
    private String imgAutoName;

    public Automobile() {
    }

    public Automobile(int idAuto, String marca, String model, LocalDate anulProducerii, String tipulCaroseriei, int pretChirie) {
        this.idAuto = idAuto;
        this.marca = marca;
        this.model = model;
        this.anulProducerii = anulProducerii;
        this.tipulCaroseriei = tipulCaroseriei;
        this.pretChirie = pretChirie;
    }

    
    public Automobile(int idAuto, String marca, String model, LocalDate anulProducerii, String tipulCaroseriei, int pretChirie, byte[] imagineAuto, String imgAutoName) {
        this.idAuto = idAuto;
        this.marca = marca;
        this.model = model;
        this.anulProducerii = anulProducerii;
        this.tipulCaroseriei = tipulCaroseriei;
        this.pretChirie = pretChirie;
        this.imagineAuto = imagineAuto;
        this.imgAutoName = imgAutoName;
    }

    public Automobile(int idAuto, String marcaAuto, String modelAuto, String tipulCaroseriei, int pretChirie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public int getPretChirie() {
        return pretChirie;
    }

    public void setPretChirie(int pretChirie) {
        this.pretChirie = pretChirie;
    }

    public int getIdAuto() {
        return idAuto;
    }

    public void setIdAuto(int idAuto) {
        this.idAuto = idAuto;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDate getAnulProducerii() {
        return anulProducerii;
    }

    public void setAnulProducerii(LocalDate anulProducerii) {
        this.anulProducerii = anulProducerii;
    }

    public String getTipulCaroseriei() {
        return tipulCaroseriei;
    }

    public void setTipulCaroseriei(String tipulCaroseriei) {
        this.tipulCaroseriei = tipulCaroseriei;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.marca);
        hash = 67 * hash + Objects.hashCode(this.model);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Automobile other = (Automobile) obj;
        if (!Objects.equals(this.marca, other.marca)) {
            return false;
        }
        if (!Objects.equals(this.model, other.model)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "Automobile{" + "idAuto=" + idAuto + ", marca=" + marca + ", model=" + model + ", anulProducerii=" + anulProducerii + ", tipulCaroseriei=" + tipulCaroseriei + ", pretChirie=" + pretChirie + '}';
    }

    public byte[] getImagineAuto() {
        return imagineAuto;
    }

    public void setImagineAuto(byte[] imagineAuto) {
        this.imagineAuto = imagineAuto;
    }

    public String getImgAutoName() {
        return imgAutoName;
    }

    public void setImgAutoName(String imgAutoName) {
        this.imgAutoName = imgAutoName;
    }
    
    

}
