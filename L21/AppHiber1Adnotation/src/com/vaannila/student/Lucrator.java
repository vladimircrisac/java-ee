/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaannila.student;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "LUCRATOR")
public class Lucrator {

    private long lucratorId;
    private String lucratorName;
    private Set<Phone> lucratorPhoneNumbers = new HashSet<Phone>(0);

    public Lucrator() {
    }

    public Lucrator(String lucratorName, Set<Phone> lucratorPhoneNumbers) {
        this.lucratorName = lucratorName;
        this.lucratorPhoneNumbers = lucratorPhoneNumbers;
    }

    @Id
    @GeneratedValue
    @Column(name = "LUCRATOR_ID")
    public long getStudentId() {
        return this.lucratorId;
    }

    public void setStudentId(long studentId) {
        this.lucratorId = studentId;
    }

    @Column(name = "LUCRATOR_NAME", nullable = false, length = 100)
    public String getLucratorName() {
        return this.lucratorName;
    }

    public void setLucratorName(String lucratorName) {
        this.lucratorName = lucratorName;
    }

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "LUCRATOR_NAME",
            joinColumns = { @JoinColumn(name = "LUCRATOR_ID")},
            inverseJoinColumns = { @JoinColumn(name = "PHONE_ID")})
    public Set<Phone> getLucratorPhoneNumbers() {
        return this.lucratorPhoneNumbers;
    }

    public void setLucratorPhoneNumbers(Set<Phone> lucratorPhoneNumbers) {
        this.lucratorPhoneNumbers = lucratorPhoneNumbers;
    }

}
