/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entitati.Admin;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author denis
 */
public class AdminDaoImpl implements AdminDaoIntf{
    private DataSource ds;

    public AdminDaoImpl(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public Admin getUserByUsernameAndPassword(String username, String password) throws IOException {
            Admin userAdmin = null;

        try (
            Connection conn = ds.getConnection();
            Statement st = conn.createStatement();){
            // SELECT * FROM carrent.admin where username = 'admin' and password = 'admin';
            String sql = "SELECT * FROM carrent.admin where username = '"+username+"' and password = '"+password+"'";
            
            ResultSet rs = st.executeQuery(sql);
            
            if (rs.next()) {
                int idAdmin = rs.getInt(1);
                boolean activated = rs.getBoolean(4);
                userAdmin = new Admin(idAdmin, username, password, activated);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(AdminDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userAdmin;
    }
    
}
