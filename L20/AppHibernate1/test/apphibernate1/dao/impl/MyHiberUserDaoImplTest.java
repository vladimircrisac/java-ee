/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphibernate1.dao.impl;

import apphibernate1.mappings.MyHiberUser;
import apphibernate1.util.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author iucosoft
 */
public class MyHiberUserDaoImplTest {

    private MyHiberUserDaoImpl instance = new MyHiberUserDaoImpl();

    @Before
    public void setUp() {
        // create table + add 1 row
        String sql = "CREATE TABLE `hiberdb`.`useri` (\n"
                + "  `id` INT NOT NULL AUTO_INCREMENT,\n"
                + "  `nume` VARCHAR(45) NULL,\n"
                + "  `prenume` VARCHAR(45) NULL,\n"
                + "  PRIMARY KEY (`id`));";

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            sess.createSQLQuery(sql).executeUpdate();
            sess.save(new MyHiberUser("TestNume1", "TestPrenume1"));

            ///////
            transaction.commit();

        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
                transaction = null;
            }
            e.printStackTrace();
        } finally {
            sess.close();
        }
    }

    // @After
    public void tearDown() {
        // drop table

        String sql = "DROP TABLE `hiberdb`.`useri`;";

        Session sess = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = sess.beginTransaction();
            ///////
            sess.createSQLQuery(sql).executeUpdate();
            ///////
            transaction.commit();

        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            sess.close();
        }

    }

    /**
     * Test of save method, of class MyHiberUserDaoImpl.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        MyHiberUser user = new MyHiberUser("TestNume2", "TestPrenume2");
        Integer expResult = 2;

        Integer result = instance.save(user);
        assertEquals(expResult, result);

        String expResultString = "TestNume2";
        String resultStr = instance.findById(2).getNume();

        assertEquals(expResultString, resultStr);
    }

    /**
     * Test of update method, of class MyHiberUserDaoImpl.
     */
    //@Test
    public void testUpdate() {
        System.out.println("update");
        MyHiberUser user = instance.findById(1);
        user.setNume("TestNume5");
        instance.update(user);

        MyHiberUser updatedUser = instance.findById(1);
        String expString = "TestNume5";
        String reusltStr = updatedUser.getNume();

        assertNotNull(reusltStr);
        assertEquals(expString, reusltStr);
    }

    /**
     * Test of delete method, of class MyHiberUserDaoImpl.
     */
    //   @Test
    public void testDelete() {
        System.out.println("delete");
        MyHiberUser user = instance.findById(1);
        instance.delete(user);
        Integer result = instance.findAll().size();
        Integer expResult = 0;
        assertEquals(expResult, result);

    }

    /**
     * Test of findById method, of class MyHiberUserDaoImpl.
     */
    // @Test
    public void testFindById() {
        System.out.println("findById");
        Integer userId = 1;
        MyHiberUser expResult = new MyHiberUser("TestNume1", "TestPrenume1");
        MyHiberUser result = instance.findById(userId);
        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class MyHiberUserDaoImpl.
     */
    // @Test
    public void testFindAll() {
        System.out.println("findAll");
        Integer expResult = 1;
        Integer result = instance.findAll().size();
        assertEquals(expResult, result);
    }

}
