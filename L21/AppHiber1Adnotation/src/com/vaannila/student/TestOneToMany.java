/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vaannila.student;

import apphiber1adnotation.util.HibernateUtil;
import java.util.HashSet;
import java.util.Set;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author iucosoft
 */
public class TestOneToMany {

    public static void main(String[] args) {

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();

            Set<Phone> phoneNumbers = new HashSet<Phone>();
            phoneNumbers.add(new Phone("house", "123456789"));
            phoneNumbers.add(new Phone("mobile", "987654321"));
                  
            Lucrator lucrator = new Lucrator("Ion", phoneNumbers);
            session.save(lucrator);

            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
            HibernateUtil.shutDown();
        }

    }
}
