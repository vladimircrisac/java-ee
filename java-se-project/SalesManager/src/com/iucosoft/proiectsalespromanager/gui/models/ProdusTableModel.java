package com.iucosoft.proiectsalespromanager.gui.models;

import com.iucosoft.proiectsalespromanager.entitati.Produs;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author iucosoft
 */
public class ProdusTableModel extends DefaultTableModel {

    String[] columns = {"ID", "Nume produs", "Barcode", "Pret", "Stock", "Producator"};

    public ProdusTableModel(List<Produs> listaProduse) {

        for (String col : columns) {
            super.addColumn(col);
        }

        for (Produs produs : listaProduse) {
            Vector row = new Vector();

            row.addElement(produs.getId());
            row.addElement(produs.getNumeProdus());
            row.addElement(produs.getBarCode());
            row.addElement(produs.getPret());
            row.addElement(produs.getStock());
            row.addElement(produs.getProducator());

            super.dataVector.add(row);
        }

    }

}
