/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitati;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author denis
 */
public class Bronare {

    private int idBronare;
//    private int idClient;
//    private int idAuto;
//    private LocalDate ridicareAuto;
//    private LocalDate intoarcereAuto;
//    private String comentariu;
    private String modelAuto;
    private String marcaAuto;
    private LocalDate startDate;
    private LocalDate endDate;
    private String numeClient;
    private String prenumeClient;
    private int experienta;
    private String comentariu;

    public Bronare() {
    }


    public Bronare(int idBronare, String modelAuto, String marcaAuto, LocalDate startDate, LocalDate endDate, String numeClient, String prenumeClient, int experienta, String comentariu) {
        this.idBronare = idBronare;
        this.modelAuto = modelAuto;
        this.marcaAuto = marcaAuto;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numeClient = numeClient;
        this.prenumeClient = prenumeClient;
        this.experienta = experienta;
        this.comentariu = comentariu;
    }

    public Bronare(String modelAuto, String marcaAuto, LocalDate startDate, LocalDate endDate, String numeClient, String prenumeClient, int experienta, String comentariu) {
        this.modelAuto = modelAuto;
        this.marcaAuto = marcaAuto;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numeClient = numeClient;
        this.prenumeClient = prenumeClient;
        this.experienta = experienta;
        this.comentariu = comentariu;
    }

    
    public int getIdBronare() {
        return idBronare;
    }

    public void setIdBronare(int idBronare) {
        this.idBronare = idBronare;
    }

    public String getModelAuto() {
        return modelAuto;
    }

    public void setModelAuto(String modelAuto) {
        this.modelAuto = modelAuto;
    }

    public String getMarcaAuto() {
        return marcaAuto;
    }

    public void setMarcaAuto(String marcaAuto) {
        this.marcaAuto = marcaAuto;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getNumeClient() {
        return numeClient;
    }

    public void setNumeClient(String numeClient) {
        this.numeClient = numeClient;
    }

    public String getPrenumeClient() {
        return prenumeClient;
    }

    public void setPrenumeClient(String prenumeClient) {
        this.prenumeClient = prenumeClient;
    }

    public int getExperienta() {
        return experienta;
    }

    public void setExperienta(int experienta) {
        this.experienta = experienta;
    }

    public String getComentariu() {
        return comentariu;
    }

    public void setComentariu(String comentariu) {
        this.comentariu = comentariu;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.idBronare;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bronare other = (Bronare) obj;
        if (this.idBronare != other.idBronare) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bronare{" + "idBronare=" + idBronare + ", modelAuto=" + modelAuto + ", marcaAuto=" + marcaAuto + ", startDate=" + startDate + ", endDate=" + endDate + ", numeClient=" + numeClient + ", prenumeClient=" + prenumeClient + ", experienta=" + experienta + ", comentariu=" + comentariu + '}';
    }

    
}
