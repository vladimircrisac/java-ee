package com.vaannila.student;

import apphiber1annotatii.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

public class TestEmbeddedOneTable {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
//			transaction = session.beginTransaction();
//			Address address = new Address("OMR Road", "Chennai", "TN", "600097");
//			Student student = new Student("Eswar", address);
//			session.save(student);
//			transaction.commit();
//                        
//                        
//                        transaction = session.beginTransaction();
//                        Student st = (Student) session.get(Student.class, 1L);
//                        
//                        Address stAddr = st.getStudentAddress();
//                        stAddr.setCity("Chisinau");
//                        
//                        transaction.commit();

//			transaction = session.beginTransaction();
//			Address address1 = new Address("OMR 1", "Chennai 1", "TN 1", "600097");
//			Student student1 = new Student("Eswar 1", address1);
//			session.save(student1);
//			transaction.commit();
//            
//			transaction = session.beginTransaction();
//			Address address2 = new Address("OMR 2", "Chennai 2", "TN 2", "600097");
//			Student student2 = new Student("Eswar 2", address2);
//			session.save(student2);
//			transaction.commit();
//            
//			transaction = session.beginTransaction();
//			Address address3 = new Address("OMR 3", "Chennai 3", "TN 3", "600097");
//			Student student3 = new Student("Eswar 3", address3);
//			session.save(student3);
//			transaction.commit();
//            
//			transaction = session.beginTransaction();
//			Address address4 = new Address("OMR 4", "Chennai 4", "TN 4", "600097");
//			Student student4 = new Student("Eswar 4", address4);
//			session.save(student4);
//			transaction.commit();
            transaction = session.beginTransaction();
            Criteria crit = session.createCriteria(Student.class);
            
            // aici se lucreaza cu campurile din Java.entitati, NU DIN SQL!
//            crit.add(Restrictions.gt("studentId", new Long(3)));


//            crit.add(Restrictions.lt("studentId", 3L));
//            List<Student> listaStudent = crit.list();
//
//            for (Student st : listaStudent) {
//                System.out.println(st);
//            }


        // Criteria by Example = cautarea in baza criteriilor - FILTRU
        Student stEx = new Student();
        stEx.setStudentName("Eswar 1");
        crit.add(Example.create(stEx));
        
        List<Student> listStud = crit.list();
            for (Student student : listStud) {
                System.out.println(student);
            }





        } catch (HibernateException e) {
            transaction.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

}
