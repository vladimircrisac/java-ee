package dao;

import entitati.Carte;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author iucosoftmain
 */
public interface CarteDaoIntf {
    
    //CRUD
    Carte getById(int id) throws SQLException;
    
    
    Carte getByIsbn(String isbn) throws SQLException;
    
    List<Carte> getAll() throws SQLException;
    
    List<Carte> getFullAll() throws SQLException; // cu imagini
    
    List<Carte> getAllByAutor(String autor) throws SQLException;
    
    List<Carte> getAllByContainIntitle(String titleCont) throws SQLException;
    
    
    void  save(Carte carte) throws SQLException;
    
    void  update(Carte carte) throws SQLException;
    
    
    void  updateCarteImg(int id, String imgName, byte[] imgBytes) throws SQLException;
    
   // Carte getCarteFullById(int id) throws SQLException;
    
    
    void  delete(Carte carte) throws SQLException;
    
    
    
}
