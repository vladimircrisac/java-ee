/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demospr1;

/**
 *
 * @author iucosoft
 */
class TextRendererConsola implements TextRenderer {

//    private TextProviderConsola provider;
    private TextProvider provider;

//    void setProvider(TextProviderConsola provider) {
//        this.provider = provider;
//    }
    public void render() {
        if (provider == null) {
            System.err.println("provider este null");
            throw new RuntimeException("err provider is null");
        }
        System.out.println(provider.getText());
    }

    @Override
    public void setProvider(TextProvider provider) {
        this.provider = provider;
    }

}
