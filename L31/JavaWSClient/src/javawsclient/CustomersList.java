/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javawsclient;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author iucosoft
 */
@XmlRootElement (name = "Customers-list")
public class CustomersList {

    private List<Customer> customerList;

    public CustomersList() {
        this.customerList = new ArrayList<>();
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    @XmlElement (name = "Customer")
    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

}
