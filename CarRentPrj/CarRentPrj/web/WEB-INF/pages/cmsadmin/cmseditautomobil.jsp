<%--
    Document   : cmseditautomobil
    Created on : Feb 25, 2019, 10:40:14 PM
    Author     : denis
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>CMS eidtare info automobil!</h1>
        <div>


            <form action="cmseditautomobilserv" method="POST">
                <p> ID <input type="text" name="id_auto" value="${auto.idAuto}" size="3" readonly="readonly"/> </p>
                <p> Marca <input type="text" name="marca_auto" value="${auto.marca}" size="40"/> </p>
                <p> Model <input type="text" name="model_auto" value="${auto.model}" size="40"/> </p>
                <p> Anul producerii <input type="text" name="anul_producerii_auto" value="${auto.anulProducerii}" size="10"/> </p>
                <!--<p> Tipul caroseriei <input type="text" name="tipul_caroseriei_auto" value="${auto.tipulCaroseriei}" size="40"/> </p>-->
                <p> Tip caroserie <select>
                        <option>sedan</option>
                        <option>hatchback</option>
                        <option>MPV</option>
                        <option>SUV</option>
                        <option>coupe</option>
                        <option>cabriolet</option>
                    </select>
                <p> Pret chirie <input type="text" name="pret_chirie_auto" value="${auto.pretChirie}" size="3"/> </p>
                <p>  <input type="submit" value="Edit Automobil (sdave or update)" />
                    <input type="reset" value="Clear" /> </p>
            </form>
        </div>
    </body>
</html>
