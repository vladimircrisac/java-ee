/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apphiber1adnotation.dao;

import apphiber1adnotation.Entities.User;
import java.util.List;


/**
 *
 * @author iucosoft
 */
public interface UserDaoIntf {
    
    Integer save(User user);
    void update(User user);
    void delete (User user);
    void deleteUserId (Integer userId);
    
    
    User findById (Integer userId);
    List<User> findAll();
}
