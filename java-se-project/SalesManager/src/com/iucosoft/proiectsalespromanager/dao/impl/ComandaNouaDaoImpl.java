/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iucosoft.proiectsalespromanager.dao.impl;

import com.iucosoft.proiectsalespromanager.dao.ClientDaoIntf;
import com.iucosoft.proiectsalespromanager.dao.ComandaNouaDaoIntf;
import com.iucosoft.proiectsalespromanager.db.MyDataSource;
import com.iucosoft.proiectsalespromanager.entitati.ComandaNoua;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author iucosoft
 */
public class ComandaNouaDaoImpl implements ComandaNouaDaoIntf {

    private static final Logger LOG = Logger.getLogger(ClientDaoIntf.class.getName());

    private MyDataSource mds = MyDataSource.getInstance();
    private Connection connection;

    @Override
    public void save(ComandaNoua comandaNoua) throws SQLException {
        //INSERT INTO `salespromanager`.`comanda_noua` (`ID_CLIENT`, `ID_PRODUS`, `CANTITATEA`, `PRET_TOTAL`, `ID_USER`, `DATA_INTOCMIRE`, `DATA_LIVRARE`, `PRET_UNITATE_CANTITATE`) VALUES ('1', '5', '2', '22', '1', '2018-12-11', '2018-12-12', '');

    }

    @Override
    public void delete(ComandaNoua comandaNoua) throws SQLException {

    }

    @Override
    public List<ComandaNoua> findAllProduse() throws SQLException {

        return null;
    }

    @Override
    public List<ComandaNoua> adaugaInCos() throws SQLException {

        return null;
    }

}
