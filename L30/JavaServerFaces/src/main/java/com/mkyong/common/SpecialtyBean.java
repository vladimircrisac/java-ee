/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong.common;

import com.mkyong.common.model.Specialty;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author iucosoft
 */

@ManagedBean
@SessionScoped
public class SpecialtyBean {
    
   private List<Specialty> specialtyList;
   private Specialty specialtyClearForm;
   
   public String goToSpecialtyPage(){
       
       specialtyList = initTestSpecialtyList();
       specialtyClearForm = new Specialty();
       
       return "specialty-page";
   }

    private List<Specialty> initTestSpecialtyList() {
        
        List specialtiesList = new ArrayList();
        
        Specialty specialty1 = new Specialty(1, "Philosophy", 20, "Dr. Watson");
        specialtiesList.add(specialty1);
        
        Specialty specialty2 = new Specialty(2, "Criminology", 10, "Sherlock Holmes");
        specialtiesList.add(specialty2);
        
        
        return specialtiesList;
    }
    
    public void editSpecialty(){
        
    }
    
    public void deleteSpecialty(Specialty specialtyToDelete){
        specialtyList.remove(specialtyToDelete);
        
    }
    
    public void saveSpecialty(){
        
    }

    public List<Specialty> getSpecialtyList() {
        return specialtyList;
    }

    public void setSpecialtyList(List<Specialty> specialtyList) {
        this.specialtyList = specialtyList;
    }

    public Specialty getSpecialtyClearForm() {
        return specialtyClearForm;
    }

    public void setSpecialtyClearForm(Specialty specialtyClearForm) {
        this.specialtyClearForm = specialtyClearForm;
    }
    
    
}
