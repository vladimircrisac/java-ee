package apphiber1annotatii.dao;

import apphiber1annotatii.entities.MyHiberUser;
import java.util.List;

/**
 *
 * @author iucosoftmain
 */
public interface MyHiberUserDaoIntf {

   //CRUD
  
    Integer  save(MyHiberUser user)   ;
    void  update(MyHiberUser user)   ;
    void  delete(MyHiberUser user)   ;
    void  delete(Integer userId)   ;
    
    
    MyHiberUser findById(Integer userId)   ;
    List<MyHiberUser> findAll()  ;
    
    
}
