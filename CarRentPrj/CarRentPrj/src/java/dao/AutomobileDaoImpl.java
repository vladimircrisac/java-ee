/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entitati.Automobile;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.DataSource;
import java.time.LocalDate;

/**
 *
 * @author iucosoft
 */
public class AutomobileDaoImpl implements AutomobileDaoIntf {

    private DataSource ds;

    public AutomobileDaoImpl(DataSource ds) {
        this.ds = ds;
    }

    @Override
    public List<Automobile> getAll() throws SQLException {
        List<Automobile> listaAuto = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement stat = conn.createStatement();

        String sql = "select * from automobile";

        ResultSet rs = stat.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listaAuto.add(automobil);
        }
        return listaAuto;

    }

    @Override
    public List<Automobile> getByCategorieSedan() throws SQLException {
        List<Automobile> listBySedan = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'sedan'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listBySedan.add(automobil);
        }
        return listBySedan;
    }

    @Override
    public List<Automobile> getByCategorieHatchback() throws SQLException {
        List<Automobile> listByHatchback = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'hatchback'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listByHatchback.add(automobil);
        }
        return listByHatchback;
    }

    @Override
    public List<Automobile> getByCategorieMPV() throws SQLException {
        List<Automobile> listByMPV = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'MPV'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listByMPV.add(automobil);
        }
        return listByMPV;
    }

    @Override
    public List<Automobile> getByCategorieSUV() throws SQLException {
        List<Automobile> listBySUV = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'SUV'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listBySUV.add(automobil);
        }
        return listBySUV;
    }

    @Override
    public List<Automobile> getByCategorieCoupe() throws SQLException {
        List<Automobile> listByCoupe = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'coupe'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = "";

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listByCoupe.add(automobil);
        }
        return listByCoupe;
    }

    @Override
    public List<Automobile> getByCategorieCabriolet() throws SQLException {
        List<Automobile> listByCabriolet = new ArrayList<>();
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "SELECT * FROM carrent.automobile where tipulcaroseriei = 'cabriolet'";

        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int idAuto = rs.getInt(1);
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null;//rs.getBlob(7);
            String imgAutoName = rs.getString(8);

            Automobile automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
            listByCabriolet.add(automobil);
        }
        return listByCabriolet;
    }

    @Override
    public Automobile getById(int idAuto) throws SQLException {
        Automobile automobil = null;
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();

        String sql = "select * from automobile where idAuto= " + idAuto;

        ResultSet rs = st.executeQuery(sql);
        if (rs.next()) {
            String marca = rs.getString(2);
            String model = rs.getString(3);
            LocalDate anulProducerii = rs.getDate(4).toLocalDate();
            String tipulCaroseriei = rs.getString(5);
            int pretChirie = rs.getInt(6);
            byte[] imagineAuto = null; //rs.getBlob(7);
            String imgAutoName = rs.getString(8);

            automobil = new Automobile(idAuto, marca, model, anulProducerii, tipulCaroseriei, pretChirie, imagineAuto, imgAutoName);
        }
        ds.getConnection().close();
        return automobil;
    }

    @Override
    public void save(Automobile automobilSave) throws SQLException {
        Connection conn = ds.getConnection();
        
        String sql = "INSERT INTO carrent.automobile (idauto, marca, model, anulproducerii, tipulcaroseriei, pretchirie, imaginenume) "
                + "VALUES (? , ? , ? , ? , ? , ? , ?)";
        
        PreparedStatement psmt = conn.prepareStatement(sql);
        psmt.setInt(1, automobilSave.getIdAuto());
        psmt.setString(2, automobilSave.getMarca());
        psmt.setString(3, automobilSave.getModel());
        psmt.setObject(4, automobilSave.getAnulProducerii());
        psmt.setString(5, automobilSave.getTipulCaroseriei());
        psmt.setInt(6, automobilSave.getPretChirie());
        psmt.setString(8, automobilSave.getImgAutoName());
        
       psmt.executeUpdate();
       ds.getConnection().close();
        
        
    }

    @Override
    public void delete(Automobile automobil) throws SQLException {
        Connection conn = ds.getConnection();
        Statement st = conn.createStatement();
        
        String sql = "DELETE FROM carrent.automobile WHERE idauto= "+automobil.getIdAuto();
        
        st.executeUpdate(sql);
        ds.getConnection().close();
    }

}
