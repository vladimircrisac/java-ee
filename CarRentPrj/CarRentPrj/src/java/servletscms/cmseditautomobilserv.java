
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servletscms;

import dao.AutomobileDaoImpl;
import dao.AutomobileDaoIntf;
import entitati.Automobile;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author denis
 */
// editare info despre auto si send la /listaautomobile.jsp
public class cmseditautomobilserv extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            DataSource ds = (DataSource) request.getServletContext().getAttribute("datasource");
            AutomobileDaoIntf automobileDao = new AutomobileDaoImpl(ds);

            int idAuto = Integer.parseInt(request.getParameter("id_auto"));
            String marcaAuto = request.getParameter("marca_auto");
            String modelAuto = request.getParameter("model_auto");
            //LocalDate anulProducerii =  request.getDateHeader("anul_producerii_auto").toLocalDate();
//            String anulProduceriiStr = request.getParameter("anul_producerii_auto");
            String tipulCaroseriei = request.getParameter("tipul_caroseriei_auto");
            int pretChirie = Integer.parseInt(request.getParameter("pret_chirie_auto"));

//            Automobile newAuto = new Automobile(idAuto, marcaAuto, modelAuto, LocalDate.MIN, tipulCaroseriei, pretChirie);
            Automobile newAuto = new Automobile(idAuto, marcaAuto, modelAuto, tipulCaroseriei, pretChirie);

            if (idAuto == 0) {
                automobileDao.save(newAuto);
            } else { // id > 0
                Automobile automobileDB = automobileDao.getById(idAuto);

                if (automobileDB != null) {
                    automobileDB.getIdAuto();
                    automobileDB.getMarca();
                    automobileDB.getModel();
                    automobileDB.getAnulProducerii();
                    automobileDB.getTipulCaroseriei();
                    automobileDB.getPretChirie();
                } else {
                    throw new SQLException("nu este automobil cu id " + idAuto);
                }
            }
            request.getRequestDispatcher("cmslistaautomobile.jsp").forward(request, response);

        } catch (SQLException ex) {
            Logger.getLogger(cmseditautomobilserv.class.getName()).log(Level.SEVERE, null, ex);
            request.setAttribute("error", ex.getMessage());

        }

    }
    
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet cmsdeleteautomobilserv</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet cmsdeleteautomobilserv at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
//    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
