
package entitati;

import java.util.Objects;

/**
 *
 * @author iucosoftmain
 */
public class Carte {
    private int id;
    private String titlul;
    private String autor;
    private String isbn;
    private int nrPagini;
    private double pret;
    private byte[] imagine;
    private String imgName;

    public Carte() {
    }

    public Carte(String titlul, String autor, String isbn, int nrPagini, double pret) {
        this.titlul = titlul;
        this.autor = autor;
        this.isbn = isbn;
        this.nrPagini = nrPagini;
        this.pret = pret;
    }

    public Carte(int id, String titlul, String autor, String isbn, int nrPagini, double pret, byte[] imagine) {
        this.id = id;
        this.titlul = titlul;
        this.autor = autor;
        this.isbn = isbn;
        this.nrPagini = nrPagini;
        this.pret = pret;
        this.imagine = imagine;
    }

    public Carte(int id, String titlul, String autor, String isbn, int nrPagini, double pret, byte[] imagine, String imgName) {
        this.id = id;
        this.titlul = titlul;
        this.autor = autor;
        this.isbn = isbn;
        this.nrPagini = nrPagini;
        this.pret = pret;
        this.imagine = imagine;
        this.imgName = imgName;
    }
    
    
    public byte[] getImagine() {
        return imagine;
    }

    public void setImagine(byte[] imagine) {
        this.imagine = imagine;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitlul() {
        return titlul;
    }

    public void setTitlul(String titlul) {
        this.titlul = titlul;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getNrPagini() {
        return nrPagini;
    }

    public void setNrPagini(int nrPagini) {
        this.nrPagini = nrPagini;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.isbn);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Carte other = (Carte) obj;
        if (!Objects.equals(this.isbn, other.isbn)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Carte{" + "titlul=" + titlul + ", autor=" + autor + ", isbn=" + isbn + ", nrPagini=" + nrPagini + ", pret=" + pret + '}';
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }
    
}
