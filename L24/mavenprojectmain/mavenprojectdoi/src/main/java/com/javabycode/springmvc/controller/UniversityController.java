/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javabycode.springmvc.controller;

import com.javabycode.springmvc.model.University;
import com.javabycode.springmvc.service.UniversityService;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author iucosoft
 */
@Controller
@RequestMapping ("/university")
public class UniversityController {
    
    
	@Autowired
	UniversityService service;
	
	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = { "/university", "/list" }, method = RequestMethod.GET)
	public String findAll (ModelMap model){
            List<University> listUniver = service.findAll();
            model.addAttribute("universities", listUniver);
            
            // dupa return se indica mapa din WEB-INF in care se afla .jsp la care ne ducem
            return "university/alluniver";
        }

	/*
	 * Add a new Student.
	 */
	@RequestMapping(value = { "/registration" }, method = RequestMethod.GET)
	public String newUniversity(ModelMap model) {
		University univer = new University();
		model.addAttribute("university", univer);
		model.addAttribute("edit", false);
		return "university/registration";
	}

	/*
	 * Handling POST request for validating the user input and saving Student in database.
	 */
	@RequestMapping(value = { "/registration" }, method = RequestMethod.POST)
	public String saveUnviersity(@Valid University univer, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return "university/registration";
		}
		
	
		
		service.save(univer);

		model.addAttribute("success", "University " + univer.getName() + " registered successfully");
		return "university/success";
	}


	/*
	 * Provide the existing Student for updating.
	 */
	@RequestMapping(value = { "/edit-{id}-university" }, method = RequestMethod.GET)
	public String editUniversity(@PathVariable int id, ModelMap model) {
		University univer = service.findById(id);
		model.addAttribute("university", univer);
		model.addAttribute("edit", true);
		return "university/registration";
	}
	
	/*
	 * Handling POST request for validating the user input and updating Student in database.
	 */
	@RequestMapping(value = { "/edit-{id}-university" }, method = RequestMethod.POST)
	public String updateUniversity(@Valid University univer, BindingResult result,
			ModelMap model, @PathVariable int id) {

		if (result.hasErrors()) {
			return "university/registration";
		}
//
//		if(!service.isStudentCodeUnique(univer.getId()er.getCode())){
//			FieldError codeError =new FieldError("Student","code",messageSource.getMessage("non.unique.code", new String[]{student.getCode()}, Locale.getDefault()));
//		    result.addError(codeError);
//			return "registration";
//		}

		service.update(univer);

		model.addAttribute("success", "University " + univer.getName()	+ " updated successfully");
		return "university/success";
	}

	
	/*
	 * Delete an Student by it's CODE value.
	 */
	@RequestMapping(value = { "/delete-{id}-university" }, method = RequestMethod.GET)
	public String deleteUniversity(@PathVariable int id) {
            University univer = service.findById(id);
            service.delete(univer);
		return "redirect:/university/list";
	}
    
}
