package dao;

import entitati.Carte;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author iucosoftmain
 */
public class CarteDaoSpringJDBCImpl implements CarteDaoIntf {

//    private DataSource ds;
    private JdbcTemplate jdbcTemplate;

//    public CarteDaoSpringJDBCImpl(DataSource jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    class CarteRowMapper implements RowMapper<Carte> {

        public Carte mapRow(ResultSet rs, int i) throws SQLException {
            return new Carte(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                    rs.getInt(5), rs.getDouble(6), rs.getBytes(7));

        }
    }

    @Override
    public Carte getById(int id) throws SQLException {
        String sql = "SELECT * FROM carti WHERE id=?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new CarteRowMapper());
    }

    @Override
    public Carte getByIsbn(String isbn) throws SQLException {

        return null;
    }

    @Override
    public List<Carte> getAll() throws SQLException {

        String sql = "SELECT * FROM carti";
        return jdbcTemplate.query(sql, new CarteRowMapper());

    }

    @Override
    public List<Carte> getAllByAutor(String autor) throws SQLException {
        return null;
    }

    @Override
    public List<Carte> getAllByContainIntitle(String titleCont) throws SQLException {

        return null;
    }

    @Override
    public void save(Carte carte) throws SQLException {

        String sql = "INSERT INTO carti (titlul, autor, isbn, nrPagini, pret) VALUES ( ?, ?, ?, ?,?);";

        jdbcTemplate.update(sql, new Object[]{
            carte.getTitlul(), carte.getAutor(), carte.getIsbn(), carte.getNrPagini(), carte.getPret()
        });

//        PreparedStatement psmt = conn.prepareStatement(sql);
//
//        psmt.setString(1, carte.getTitlul());
//        psmt.setString(2, carte.getAutor());
//
//        psmt.setString(3, carte.getIsbn());
//
//        psmt.setInt(4, carte.getNrPagini());
//        psmt.setDouble(5, carte.getPret());
//
//// cu imaginea... +++
//        psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
//        jdbcTemplate.getConnection().close();
    }

    @Override
    public void update(Carte carte) throws SQLException {

        //UPDATE `ANGAJATIDB`.`CARTI` SET `titlul`='www', `autor`='fff', `isbn`='222', `nrPagini`='1', `pret`='11.11122' WHERE `id`='7';
        String sql = "UPDATE carti SET titlul=?, autor=?, isbn=?, nrPagini=?, pret=? WHERE id=?;";

//        PreparedStatement psmt = conn.prepareStatement(sql);
//
//        psmt.setString(1, carte.getTitlul());
//        psmt.setString(2, carte.getAutor());
//
//        psmt.setString(3, carte.getIsbn());
//
//        psmt.setInt(4, carte.getNrPagini());
//        psmt.setDouble(5, carte.getPret());
//        psmt.setInt(6, carte.getId());
//
//// cu imaginea... +++
//        psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
//        jdbcTemplate.getConnection().close();
    }

    @Override
    public void delete(Carte carte) throws SQLException {

        String sql = "DELETE FROM carti WHERE id=" + carte.getId();

        jdbcTemplate.update(sql, new Object[]{carte.getId()});
    }

    @Override
    public List<Carte> getFullAll() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateCarteImg(int id, String imgName, byte[] imgBytes) throws SQLException {

        String sql = "UPDATE carti SET  imagine=?, imgName=?  WHERE id=?;";

        jdbcTemplate.update(sql, new Object[]{imgBytes, imgName, id});
//
//        try (
//                Connection conn = jdbcTemplate.getConnection();
//                PreparedStatement psmt = conn.prepareStatement(sql);) {
//
//            psmt.setBytes(1, imgBytes);
//            psmt.setString(2, imgName);
//            psmt.setInt(3, id);
//
//// cu imaginea... +++
//            psmt.executeUpdate();// ATENTI E Aici este fara SQL!!!!! prepared st
//            //   jdbcTemplate.getConnection().close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new SQLException(e.getMessage());
//        }

    }

}
