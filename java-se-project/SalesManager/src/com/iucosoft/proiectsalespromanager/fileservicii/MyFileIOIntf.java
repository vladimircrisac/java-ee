package com.iucosoft.proiectsalespromanager.fileservicii;

public interface MyFileIOIntf<T> extends MyWritable<T>, MyReadable<T> {

}
