/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeneri;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

/**
 * Web application lifecycle listener.
 *
 * @author iucosoft
 */
public class CarRentServletListener implements ServletContextListener {

    private static final Logger LOG = Logger.getLogger(CarRentServletListener.class.getName());

    private Integer contorSesiuni = 0;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            LOG.info("Contextul a fost creat - s-a facut deploy pe webserver (Tomcat)");
            sce.getServletContext().setAttribute("contorSesiuni", contorSesiuni);

            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/refdb");

            if (ds != null) {
                Connection conn = ds.getConnection();
                if (conn != null) {
                    LOG.info("connected to DB - OK!");
                    conn.close();
                    sce.getServletContext().setAttribute("datasource", ds);
                }
            } else {
                LOG.severe("nu pot obtine DATA SOURCE" + ds);
            }
        } catch (NamingException ex) {
            Logger.getLogger(CarRentServletListener.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CarRentServletListener.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute("contorSesiuni");

        DataSource ds = (DataSource) sce.getServletContext().getAttribute("datasource");
        if (ds != null) {
            try {
                Connection conn = ds.getConnection();
                if (conn != null) {
                    conn.close();
                    sce.getServletContext().removeAttribute("datasource");
                }
            } catch (SQLException ex) {
                Logger.getLogger(CarRentServletListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        LOG.info("Contextul a fost distrus - s-a facut undeploy de pe webserver (Tomcat)");

    }
}
