/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entitati;

/**
 *
 * @author denis
 */
public class Clienti {

    private int idClient;
    private String nume;
    private String prenume;
    private int nrTelefon;
    private int experienta;

    public Clienti() {
    }

    public Clienti(int idClient, String nume, String prenume, int nrTelefon, int experienta) {
        this.idClient = idClient;
        this.nume = nume;
        this.prenume = prenume;
        this.nrTelefon = nrTelefon;
        this.experienta = experienta;
    }

    public int getExperienta() {
        return experienta;
    }

    public void setExperienta(int experienta) {
        this.experienta = experienta;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public int getNrTelefon() {
        return nrTelefon;
    }

    public void setNrTelefon(int nrTelefon) {
        this.nrTelefon = nrTelefon;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.idClient;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Clienti other = (Clienti) obj;
        if (this.idClient != other.idClient) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clienti{" + "idClient=" + idClient + ", nume=" + nume + ", prenume=" + prenume + ", nrTelefon=" + nrTelefon + ", experienta=" + experienta + '}';
    }

}
