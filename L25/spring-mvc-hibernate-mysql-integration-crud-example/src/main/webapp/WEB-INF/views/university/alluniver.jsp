<%-- 
    Document   : alluniver
    Created on : Apr 11, 2019, 8:26:53 PM
    Author     : iucosoft
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            tr:first-child{
                font-weight: bold;
                background-color: #C6C9C4;
            }
        </style>
    </head>
    <body>
        <h1>List of universities!</h1>

        <table>
            <tr>
                <td>ID</td><td>Name</td><td>Address</td><td>EDIT</td>
            </tr>
            <c:forEach items="${universities}" var="university">
                <tr>
                    <td>${university.id}</td>
                    <td>${university.name}</td>
                    <td>${university.address}</td>
                    <td><a href="<c:url value='/university/edit-${university.id}-university' />">${university.id}</a></td>
                    <td><a href="<c:url value='/university/delete-${university.id}-university' />">delete</a></td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <a href="<c:url value='/university/registration'/>"> Add new university</a>
        



    </body>
</html>
